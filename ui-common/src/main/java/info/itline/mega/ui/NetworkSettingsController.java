package info.itline.mega.ui;

import info.itline.mega.net.Client;
import info.itline.mega.net.DeviceId;
import info.itline.mega.net.IpAddress;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.StringExpression;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Window;

public class NetworkSettingsController extends SettingsGroup implements Initializable {

    public static Form<NetworkSettingsController> load() {
        try {
            FXMLLoader loader = new FXMLLoader(
                    NetworkSettingsController.class.getResource(
                            "fxml/NetworkSettingsPane.fxml"));
            loader.load();
            return new Form<>(
                    (NetworkSettingsController) loader.getController(), 
                    (Pane) loader.getRoot());
        }
        catch (IOException ignore) {
            throw new RuntimeException(ignore);
        }
    }

    @Override
    public String getGroupName() {
        return "Сетевые настройки";
    }

    @Override
    public String getIconFilename() {
        return "network.png";
    }
    
    @Override
    protected void updateView(DeviceId id) {
        if (id != null) {
            System.out.println(getDeviceId());
            Client.getInstance().getNetworkSetings(
                    getDeviceId(), 
                    (p) -> {
                        addressField.setText(p.getIpAddress().toString());
                        maskField.setText(p.getNetmask().toString());
                        gatewayField.setText(p.getGateway().toString());
                        nameField.setText(p.getName());
                    }, null);
        }
        else {
            addressField.setText("");
            maskField.setText("");
            gatewayField.setText("");
            nameField.setText("");
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        BooleanBinding isInputValid = 
                new IsIpAddress(addressField.textProperty())
                .and(new IsIpAddress(maskField.textProperty()))
                .and(new IsIpAddress(gatewayField.textProperty()))
                .and(nameField.textProperty().isNotEqualTo(""));
        saveButton.disableProperty().bind(isInputValid.not());
    }

    @Override
    public Window getWindow() {
        return saveButton.getScene().getWindow();
    }

    @Override
    protected UpdateOperations getOperationType() {
        return UpdateOperations.CHANGE_NETWORK_SETTINGS;
    }
    
    @FXML
    private void saveButtonClicked(MouseEvent t) {
        getPasswordAndExecute(
                (password, handler) -> {
                    Client.getInstance().setNetworkSettings(getDeviceId(), 
                            IpAddress.parse(addressField.getText()), 
                            IpAddress.parse(maskField.getText()), 
                            IpAddress.parse(gatewayField.getText()), 
                            nameField.getText(), 
                            defaultReceiveHandler, handler);
                });
    }
    
    private static class IsIpAddress extends BooleanBinding {

        public IsIpAddress(StringExpression e) {
            bind(e);
            string = e;
        }

        @Override
        protected boolean computeValue() {
            return string.get() != null && IpAddress.isAddress(string.get());
        }
        
        private final StringExpression string;
    }
    
    @FXML
    private TextField addressField, maskField, gatewayField, nameField;
    @FXML
    private Button saveButton;
}
