package info.itline.mega.ui;

import info.itline.config.bankboard.ConfigManager;
import info.itline.config.bankboard.DeviceConfig;
import info.itline.config.cluster.Clusters;
import info.itline.mega.net.Client;
import info.itline.mega.net.DeviceId;
import info.itline.mega.net.DeviceType;
import info.itline.mega.net.IpAddress;
import info.itline.mega.net.MacAddress;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.control.Accordion;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.Window;

public class DeviceSelectorController implements Initializable {

   public static Form<DeviceSelectorController> load() {
      try {
         FXMLLoader loader = new FXMLLoader(
            DeviceSelectorController.class.getResource(
               "fxml/DeviceSelector.fxml"));
         loader.load();
         return new Form<>(
            (DeviceSelectorController) loader.getController(),
            (Pane) loader.getRoot());
      }
      catch (IOException ignore) {
         throw new RuntimeException(ignore);
      }
   }

   @Override
   public void initialize(URL url, ResourceBundle rb) {
      accordion.disableProperty().bind(
         deviceCombo.getSelectionModel().selectedItemProperty().isNull());
      broadcastRequestsCheckbox.selectedProperty().addListener(
         (o, oldValue, newValue) -> {
            Client.getInstance().setSendBroadcastPackets(newValue);
         });
      Client.getInstance().setSendBroadcastPackets(
         broadcastRequestsCheckbox.isSelected());

      rootPane.setOnDragDetected((t) -> {
         Window w = getWindow();
         dragOrigin = new Point2D(t.getScreenX(), t.getScreenY());
         sceneOrigin = new Point2D(w.getX(), w.getY());
         rootPane.startFullDrag();
      });

      rootPane.setOnMouseDragOver((t) -> {
         double dx = t.getScreenX() - dragOrigin.getX();
         double dy = t.getScreenY() - dragOrigin.getY();
         Window w = getWindow();
         w.setX(sceneOrigin.getX() + dx);
         w.setY(sceneOrigin.getY() + dy);
      });

      if (ApplicationLaunchHelper.IS_CLUSTER_BOARD) {
         broadcastRequestsCheckbox.setSelected(false);
         broadcastRequestsCheckbox.setVisible(false);
      }
   }

   public void setAskPassword(boolean v) {
      askPassword = v;
      for (SettingsGroup g : controllers) {
         g.setAskPassword(v);
      }
   }

   public boolean getAskPassword() {
      return askPassword;
   }

   public boolean getUseConfig() {
      return useConfig;
   }

   public void setUseConfig(boolean v) {
      useConfig = v;
      for (SettingsGroup g : controllers) {
         g.setUseConfig(v);
      }
   }

   public DeviceType getDeviceType() {
      return deviceType;
   }

   public void setDeviceType(DeviceType deviceType) {
      this.deviceType = deviceType;
   }

   public void setChildren(Form<? extends SettingsGroup>... chidren) {
      if (controllers != null) {
         for (SettingsGroup g : controllers) {
            g.deviceIdProperty().unbind();
         }
      }

      controllers = new ArrayList<>();
      accordion.getPanes().clear();

      for (Form<? extends SettingsGroup> form : chidren) {
         SettingsGroup c = form.getController();
         c.deviceIdProperty().bind(
            deviceCombo.getSelectionModel().selectedItemProperty());
         c.setUseConfig(useConfig);
         c.setAskPassword(askPassword);
         controllers.add(form.getController());

         c.setAvalableDeviceIds(deviceCombo.itemsProperty());
         c.onPostLoad();

         Pane root = form.getRootNode();
         root.getStyleClass().add("accordion-pane");
         BorderPane bp = new BorderPane();
         bp.setCenter(root);

         TitledPane tp = new TitledPane(c.getGroupName(), root);
         tp.setGraphicTextGap(10);

         ImageView iv = new ImageView(
            DeviceSelectorController.class.getResource(
               "img/" + c.getIconFilename()).toString());
         iv.setFitHeight(16);

         tp.setGraphic(iv);
         accordion.getPanes().add(tp);
      }

      if (chidren.length != 0) {
         accordion.setExpandedPane(accordion.getPanes().get(0));
      }
   }

   @FXML
   private void searchButtonClicked(MouseEvent t) {
      searchForDevices();
   }

   public void searchForDevices() {
      if (deviceType == null) {
         throw new IllegalStateException();
      }
      deviceCombo.getItems().clear();
      findOnlineDevices();
      pollDevicesFromConfig();
      pollClusterDevices();
   }

   private void findOnlineDevices() {
      Client.getInstance().search(
         getDeviceType(),
         p -> {
            DeviceId id = new DeviceId(p);
            deviceCombo.getItems().add(id);
            // 
//                    deviceCombo.getItems().add(id);
            // 
            deviceCombo.getSelectionModel().select(0);
            fireDeviceFoundEvent(id);
         },
         (c, m) -> {
            PopupHelper.showPopup(m, accordion.getScene().getWindow());
         },
         null);
//        for (int i = 0; i < 5; i++) {
//            deviceCombo.getItems().add(new DeviceId(DeviceType.MEGA_BOARD, 
//                    IpAddress.broadcastAddress, MacAddress.broadcastAddress, ""));
//        }
      deviceCombo.getSelectionModel().select(0);
   }

   private void pollDevicesFromConfig() {
      if (!useConfig) {
         return;
      }
      for (DeviceConfig cfg : ConfigManager.getManager().getAllConfigs()) {
         DeviceId id = new DeviceId(deviceType,
            cfg.getIpAddress() != null ? cfg.getIpAddress() : IpAddress.broadcastAddress, cfg.getMacAddress(), "");
         Client.getInstance().getDeviceInfo(
            id,
            p -> {
               DeviceId rid = new DeviceId(p);
               if (!deviceAlreadyFound(deviceCombo.getItems(), rid)) {
                  deviceCombo.getItems().add(rid);
                  deviceCombo.getSelectionModel().select(0);
                  fireDeviceFoundEvent(id);
               }
            },
            null);
      }
   }

   private void pollClusterDevices() {
      if (!ApplicationLaunchHelper.IS_CLUSTER_BOARD) {
         return;
      }

      for (List<DeviceId> cluster : Clusters.getInstance().getAllClusters()) {
         for (DeviceId id : cluster) {
            Client.getInstance().getDeviceInfo(
               id,
               p -> {
                  DeviceId rid = new DeviceId(p);
                  if (!deviceAlreadyFound(deviceCombo.getItems(), rid)) {
                     deviceCombo.getItems().add(rid);
                     deviceCombo.getSelectionModel().select(0);
                     fireDeviceFoundEvent(id);
                  }
               },
               null);
         }
      }
   }

   private boolean deviceAlreadyFound(List<DeviceId> list, DeviceId s) {
      for (DeviceId o : list) {
         if (o.getMacAddress().equals(s.getMacAddress())) {
            return true;
         }
      }
      return false;
   }

   private void fireDeviceFoundEvent(DeviceId id) {
      for (SettingsGroup g : controllers) {
         if (g.getDeviceFoundHandler() != null) {
            g.getDeviceFoundHandler().handle(id);
         }
      }
   }

   @FXML
   private void minimizeButtonClicked(ActionEvent t) {
      ((Stage) getWindow()).setIconified(true);
   }

   @FXML
   private void exitButtonClicked(ActionEvent t) {
      System.exit(0);
   }

   private Window getWindow() {
      return rootPane.getScene().getWindow();
   }

   @FXML
   private ComboBox<DeviceId> deviceCombo;
   @FXML
   private CheckBox broadcastRequestsCheckbox;
   @FXML
   private Accordion accordion;

   @FXML
   private Pane rootPane;

//    private final List<DeviceId> devices = new LinkedList<>();
   private List<SettingsGroup> controllers = new LinkedList<>();
   private DeviceType deviceType;

   private boolean askPassword;
   private boolean useConfig;

   private Point2D dragOrigin;
   private Point2D sceneOrigin;
}
