package info.itline.mega.ui;

import javafx.animation.FadeTransition;
import javafx.animation.FadeTransitionBuilder;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.LabelBuilder;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Popup;
import javafx.stage.PopupBuilder;
import javafx.stage.Window;
import javafx.util.Duration;

public class PopupHelper {

    public static Popup showPopup(String message, final Window window) {
        Label label;
        final Popup result = PopupBuilder.create()
                .autoFix(true)
                .autoHide(true)
                .hideOnEscape(true)
                .content(
                        label = LabelBuilder.create()
                        .text(message)
                        .stylesheets(PopupHelper.class.getResource(
                                        "css/popup.css").toString())
                        .styleClass("popup")
                        .build()
                )
                .build();

        result.setConsumeAutoHidingEvents(false);
        result.show(window);

        double x = window.getX() + window.getWidth() / 2
                - result.getWidth() / 2;
        double y = window.getY() + window.getHeight()
                - result.getHeight() - POPUP_BOTTOM_OFFSET;
        result.setX(x);
        result.setY(y);

        Rectangle clip = new Rectangle(result.getWidth(), result.getHeight());
        clip.setArcHeight(10);
        clip.setArcWidth(10);
        result.getScene().getRoot().setClip(clip);
        result.getScene().setFill(Color.TRANSPARENT);

        FadeTransition fade = FadeTransitionBuilder.create()
                .node(label)
                .duration(FADE_DURATION)
                .delay(VISIBILITY_DURATION)
                .fromValue(1.0f)
                .toValue(0.0f)
                .onFinished(t -> result.hide())
                .build();
        fade.play();

        return result;
    }

    private static final int POPUP_BOTTOM_OFFSET = 75;
    private static final Duration VISIBILITY_DURATION = new Duration(3000),
            FADE_DURATION = new Duration(500);
}
