package info.itline.mega.ui;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.StringExpression;
import javafx.scene.control.TextField;

public class StringValidatorBinding extends BooleanBinding {
    
    public StringValidatorBinding(TextField f) {
        this.expr = f.textProperty();
        bind(expr);
    }

    public static StringValidatorBinding isNonEmptyString(TextField f) {
        return new StringValidatorBinding(f);
    }
    
    @Override
    protected boolean computeValue() {
        return !expr.getValue().isEmpty();
    }
    
    private final StringExpression expr;
}
