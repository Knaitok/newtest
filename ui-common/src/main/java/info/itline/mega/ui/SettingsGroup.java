package info.itline.mega.ui;

import info.itline.mega.net.Client;
import info.itline.mega.net.DeviceId;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.stage.Window;

public abstract class SettingsGroup implements Initializable {

    public SettingsGroup() {
        deviceId.addListener((ov, oldValue, newValue) -> {
            updateView(newValue);
        });
    }
    
    protected static interface Executor {
        void execute(int password, Client.OnError defaultErrorHandler);
    }
    
    protected void getPasswordAndExecute(Executor e) {
        PasswordStorage passwordStorage = PasswordStorage.getStorage();
        PasswordType pt = getOperationType().getPasswordType();
        DeviceId id = getDeviceId();
        
        int password;
        if (getAskPassword()) {
            if (passwordStorage.havePassword(id, pt)) {
                password = passwordStorage.getPassword(id, pt);
            }
            else {
                password = PasswordDialogController.askForPassword(getWindow());
                if (password == -1) {
                    return;
                }
                passwordStorage.setPassword(id, pt, password);
            }
        }
        else {
            password = -1;
        }
        e.execute(password, defaultErrorHandler);
    }
    
    public boolean getUseConfig() {
        return useConfig;
    }
    
    public void setUseConfig(boolean v) {
        useConfig = v;
    }
    
    public boolean getAskPassword() {
        return askPassword;
    }
    
    public void setAskPassword(boolean v) {
        askPassword = v;
    }
    
    public synchronized DeviceId getDeviceId() {
        return deviceId.get();
    }
    
    public synchronized void setDeviceId(DeviceId d) {
        deviceId.set(d);
    }
    
    public List<DeviceId> getAvailableDevicesIds() {
        return availableDevices.get();
    }
    
    public void setAvalableDeviceIds(ObjectProperty<ObservableList<DeviceId>> ids) {
        availableDevices = ids;
    }
    
    public ObjectProperty<DeviceId> deviceIdProperty() {
        return deviceId;
    }
    
    public abstract String getGroupName();
    public abstract Window getWindow();
    protected abstract void updateView(DeviceId settings);
    protected abstract UpdateOperations getOperationType();
    public abstract String getIconFilename();
    
    public void onPostLoad() {
        
    }
    
    public static interface OnDeviceFound {
        
        void handle(DeviceId id);
    }
    
    public OnDeviceFound getDeviceFoundHandler() {
        return deviceFoundHandler;
    }
    
    public void setDeviceFoundHandler(OnDeviceFound handler) {
        deviceFoundHandler = handler;
    }
    
    protected final Client.OnError defaultErrorHandler = (c, d) -> {
        PopupHelper.showPopup(d, getWindow());
        if (c == Client.ErrorCode.WRONG_PASSWORD) {
            PasswordStorage.getStorage().removePassword(
                    getDeviceId(), getOperationType().getPasswordType());
        }
    };
    
    protected final Client.OnReceive defaultReceiveHandler = 
            p -> PopupHelper.showPopup("Сохранено: " + p.getMacAddress(), getWindow());
    
    private boolean useConfig;
    private boolean askPassword;
    private final ObjectProperty<DeviceId> deviceId = new SimpleObjectProperty();
    private ObjectProperty<ObservableList<DeviceId>> availableDevices;
    private OnDeviceFound deviceFoundHandler;
}
