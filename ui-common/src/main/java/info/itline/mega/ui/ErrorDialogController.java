package info.itline.mega.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class ErrorDialogController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        root.setOnKeyPressed(
                t -> {
                    KeyCode code = t.getCode();
                    if (code == KeyCode.ENTER || code == KeyCode.ESCAPE) {
                        hide();
                    }
                    t.consume();
                });
    }
    
    public static void showAndWait(Window owner, String message) {
        try {
            FXMLLoader loader = new FXMLLoader(
                    ErrorDialogController.class.getResource(
                            "fxml/ErrorDialog.fxml"));
            loader.load();
            Stage stage = FxDialogHelper.createModalStage(
                    (Pane) loader.getRoot(), owner, 
                    StageStyle.UTILITY, "Сообщение");
            ErrorDialogController ctl = loader.getController();
            ctl.setMessage(message);
            stage.showAndWait();
        }
        catch (IOException ignore) {
            
        }
    }
    
    private void setMessage(String message) {
        messageLabel.setText(message);
    }
    
    @FXML
    private void okButtonClicked(ActionEvent t) {
        hide();
    }
    
    private void hide() {
        root.getScene().getWindow().hide();
    }
    
    @FXML
    private Label messageLabel;
    @FXML
    private Pane root;
}
