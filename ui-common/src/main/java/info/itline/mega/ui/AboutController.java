package info.itline.mega.ui;

import info.itline.mega.net.DeviceId;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.Pane;
import javafx.stage.Window;

public class AboutController extends SettingsGroup implements Initializable {

    public static Form<AboutController> load() {
        return FxmlLoadingHelper.load(AboutController.class, "fxml/AboutPane.fxml");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @Override
    protected void updateView(DeviceId settings) {
    }

    @Override
    public Window getWindow() {
        return root.getScene().getWindow();
    }

    @Override
    public String getGroupName() {
        return "О программе";
    }

    @Override
    public String getIconFilename() {
        return "about.png";
    }
    
    @FXML
    private void siteLinkClicked(ActionEvent t) {
        ApplicationHolder.theApplication.getHostServices()
                .showDocument(siteLink.getText());
    }

    @Override
    protected UpdateOperations getOperationType() {
        throw new UnsupportedOperationException();
    }
    
    @FXML
    private Pane root;
    @FXML
    private Hyperlink siteLink;
}
