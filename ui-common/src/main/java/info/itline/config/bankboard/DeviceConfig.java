package info.itline.config.bankboard;

import info.itline.mega.net.IpAddress;
import info.itline.mega.net.MacAddress;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class DeviceConfig {
    DeviceConfig(List<String> currencyNames, List<String> additionalInfo, 
            MacAddress macAddress, int digitCount, int dotPosition, 
            boolean haveCreepingLine, int creepingLineLength, 
            IpAddress ipAddress, List<String> crossNames, 
            int crossDigitCount, int crossDotPosition) 
            throws Exceptions.WrongDeviceSettings {
        if (currencyNames.size() > MAX_CURRENCY_ENTRY_COUNT / 2) {
            wrongConfigurationError("Указано слишком много валют");
        }   
        this.currencyNames = currencyNames;
        
        this.crossNames = crossNames;
        if (crossNames.size() * 2 + currencyNames.size() > MAX_CURRENCY_ENTRY_COUNT) {
            wrongConfigurationError("Указано слишком много полей");
        }
        
        if (crossDigitCount > DIGIT_COUNT_MAX || crossDigitCount < DIGIT_COUNT_MIN)
            throw new Exceptions.WrongDeviceSettings("Недопустимое количество цифр");
        this.digitCount = digitCount;
        if (crossDotPosition > DOT_POSITION_MAX || crossDotPosition < DOT_POSITION_MIN || 
                crossDotPosition >= crossDigitCount)
            throw new Exceptions.WrongDeviceSettings("Недопустимое положение десятичной точки");
        
        this.crossDigitCount = crossDigitCount;
        this.crossDotPosition = crossDotPosition;
        
        if (additionalInfo.size() > MAX_ADDITIONAL_INFO_COUNT) {
            wrongConfigurationError("Слишком много записей для дополнтельной информации");
        }
        this.additionalInfo = additionalInfo;
        
        this.macAddress = macAddress;
        if (digitCount > DIGIT_COUNT_MAX || digitCount < DIGIT_COUNT_MIN)
            throw new Exceptions.WrongDeviceSettings("Недопустимое количество цифр");
        this.digitCount = digitCount;
        if (dotPosition > DOT_POSITION_MAX || dotPosition < DOT_POSITION_MIN || 
                dotPosition >= digitCount)
            throw new Exceptions.WrongDeviceSettings("Недопустимое положение десятичной точки");
        this.dotPosition = dotPosition;
        this.haveCreepingLine = haveCreepingLine;
        this.creepingLineLength = creepingLineLength;
        this.ipAddress = ipAddress;
    }
    
    private static void wrongConfigurationError(String message) 
            throws Exceptions.WrongDeviceSettings{
        Exceptions.WrongDeviceSettings exc = new Exceptions.WrongDeviceSettings(message);
        log.severe(exc.getMessage());
        throw exc;
    }
    
    public int getCurrencyNamesCount() {
        return currencyNames.size();
    }
    
    public String getCurrencyName(int i) {
        return currencyNames.get(i);
    }
    
    public int getAdditionalInfoCount() {
        return additionalInfo.size();
    }
    
    public String getAdditionalInfoDesription(int i) {
        return additionalInfo.get(i);
    }
    
    public MacAddress getMacAddress() {
        return macAddress;
    }
    
    public IpAddress getIpAddress() {
        return ipAddress;
    }
    
    public int getDigitCount() {
        return digitCount;
    }

    public int getDotPosition() {
        return dotPosition;
    }

    public boolean getHaveCreepingLine() {
        return haveCreepingLine;
    }

    public int getCreepingLineLength() {
        return creepingLineLength;
    }

    public List<String> getCrossNames() {
        return Collections.unmodifiableList(crossNames);
    }

    public int getCrossDigitCount() {
        return crossDigitCount;
    }

    public int getCrossDotPosition() {
        return crossDotPosition;
    }
    
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append(String.format("[Mac = %s, Digit count = %d, Dot position = %d, "
                + "Have creeping line = %b, Creeping line length = %d, ",
                macAddress, digitCount, dotPosition, haveCreepingLine, creepingLineLength));
        printList(currencyNames, "Currency", buf);
        buf.append(", ");
        printList(additionalInfo, "Additional", buf);
        buf.append("]");
        return buf.toString();
    }
    
    private static void printList(List<String> lst, String name, StringBuilder buf) {
        buf.append(name).append(" = { ");
        for (String s: lst) {
            buf.append(s).append(" ");
        }
        buf.append("}");
    }
    
    public static final int MAX_CURRENCY_ENTRY_COUNT = 32;
    public static final int MAX_ADDITIONAL_INFO_COUNT = 32;
    public static final int DIGIT_COUNT_MAX = 8;
    public static final int DIGIT_COUNT_MIN = 2;
    public static final int DOT_POSITION_MAX = 7;
    public static final int DOT_POSITION_MIN = 1;
    
    private List<String> currencyNames;
    private List<String> crossNames;
    private List<String> additionalInfo;
    private MacAddress macAddress;
    private IpAddress ipAddress;
    private int digitCount, dotPosition;
    private int crossDigitCount, crossDotPosition;
    private boolean haveCreepingLine;
    private int creepingLineLength;
    
    private static final Logger log = Logger.getLogger(DeviceConfig.class.getName());
}
