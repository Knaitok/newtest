package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
public class NetworkSettingsPacket extends Packet {

    private NetworkSettingsPacket() {
        this.name = null;
        this.ipAddress = null;
        this.netmask = null;
        this.gateway = null;
    }

    NetworkSettingsPacket(String name, IpAddress ipAddress, IpAddress netmask, IpAddress gateway) {
        assert(name.length() < NAME_LENGTH);
        setPacketType(PacketType.NETWORK_SETTINGS);
        this.name = name;
        this.ipAddress = ipAddress;
        this.netmask = netmask;
        this.gateway = gateway;
    }

    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.NETWORK_SETTINGS);
    }

    public String getName() {
        return name;
    }

    public IpAddress getIpAddress() {
        return ipAddress;
    }

    public IpAddress getNetmask() {
        return netmask;
    }

    public IpAddress getGateway() {
        return gateway;
    }
    
    @Serialize(offset = 0, maxLength = NAME_LENGTH)
    private final String name;
    @Serialize(offset = 1)
    private final IpAddress ipAddress;
    @Serialize(offset = 2)
    private final IpAddress netmask;
    @Serialize(offset = 3)
    private final IpAddress gateway;
    
    public static final int NAME_LENGTH = 32;
}
