package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializer;
import info.itline.binaryserializer.PostCreateException;
import info.itline.mega.net.handlers.NettyRequestHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;

import java.io.IOException;
import java.util.Map;

class NettyClientHandler extends SimpleChannelInboundHandler<DatagramPacket> {

    private final BinarySerializer serializer = new BinarySerializer("windows-1251");
    private final Map<Integer, NettyRequestHandler> requests;

    NettyClientHandler(Map<Integer, NettyRequestHandler> requests) {
        this.requests = requests;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) throws Exception {
        byte[] bytes = new byte[msg.content().readableBytes()];
        msg.content().getBytes(0, bytes);
        Packet packet = serializer.deserialize(Packet.class, bytes);
        try {
            NettyRequestHandler handler = requests.get(packet.getId());
            if (handler != null) {
                packet = serializer.deserialize(handler.getPacketClass(), bytes);
                requests.get(packet.getId()).fireOnReceive(packet);
            }
        } catch (PostCreateException |IOException e) {
            requests.get(packet.getId()).fireOnError(e.getMessage());
        } finally {
            requests.remove(packet.getId());
        }
    }
}