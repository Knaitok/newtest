package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
class GetPersistentNumberPacket extends Packet {

    public GetPersistentNumberPacket(short[] idx) {
        this.idx = new short[PersistentNumberPacket.INDEX_COUNT];
        int len = idx.length;
        int freeItemCount = PersistentNumberPacket.INDEX_COUNT - len;
        System.arraycopy(idx, 0, this.idx, 0, len);
        for (int i = 0; i < freeItemCount; i++) {
            this.idx[len + i] = InvalidItemIndex.SHORT_VALUE;
        }
        setPacketType(PacketType.GET_PERSISTENT_NUMBER);
    }

    public short[] getIdx() {
        return idx;
    }
    
    @Serialize(offset = 0, itemCount = PersistentNumberPacket.INDEX_COUNT)
    private final short[] idx;
}
