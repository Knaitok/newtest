package info.itline.mega.net;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeParser {

    private TimeParser() {
    }
    
    public static int parseTimeMmSs(String time) {
        Matcher m = MM_SS.matcher(time);
        int mm, ss;
        if (!m.matches()) {
            throw new IllegalArgumentException("Not a time: " + time);
        }
        
        mm = Integer.parseInt(m.group(1));
        ss = Integer.parseInt(m.group(2));
        
        if (ss > MAX_SECOND) {
            throw new IllegalArgumentException("Not a time: " + time);
        }
        
        return mm * SECOND_PER_MINUTE + ss;
    }
    
    public static int parseTime(String time) {
        Matcher m = HH_MM_SS.matcher(time);
        int hh, mm, ss;
        if (!m.matches()) {
            m = MM_SS.matcher(time);
            if (!m.matches()) {
                throw new IllegalArgumentException("Not a time: " + time);
            }
            hh = 0;
            mm = Integer.parseInt(m.group(1));
            ss = Integer.parseInt(m.group(2));
        }
        else {
            hh = Integer.parseInt(m.group(1));
            mm = Integer.parseInt(m.group(2));
            ss = Integer.parseInt(m.group(3));
        }
        if (hh > MAX_HOUR || mm > MAX_MINUTE || ss > MAX_SECOND) {
            throw new IllegalArgumentException("Value out or range: " + time);
        }
        return hh * SECOND_PER_HOUR + mm * SECOND_PER_MINUTE + ss;
    }
    
    public static String formatTime(int seconds) {
        int hh = seconds / SECOND_PER_HOUR;
        seconds %= SECOND_PER_HOUR;
        int mm = seconds / SECOND_PER_MINUTE;
        int ss = seconds % SECOND_PER_MINUTE;
        
        if (hh != 0) {
            return String.format("%2d:%02d:%02d", hh, mm, ss);
        }
        else {
            return String.format("%2d:%02d", mm, ss);
        }
    }
    
    public static String formatTimeMmSs(int seconds) {
        return String.format("%2d:%02d", seconds / SECOND_PER_MINUTE, seconds % SECOND_PER_MINUTE);
    }
    
    public static String formatTimeMmSS(int seconds) {
       int mm = seconds / 60;
       int ss = seconds % 60;
       
       return String.format("%02d:%02d", mm, ss);
    }
    
    private static final Pattern 
            HH_MM_SS    = Pattern.compile("\\s*([0-9]{1,2})\\s*:\\s*([0-9]{2})\\s*:\\s*([0-9]{2})\\s*"),
            MM_SS       = Pattern.compile("\\s*([0-9]{1,2})\\s*:\\s*([0-9]{2})\\s*");
    
    private static final int 
            SECOND_PER_MINUTE = 60,
            SECOND_PER_HOUR = SECOND_PER_MINUTE * 60,
            MAX_HOUR = 99,
            MAX_MINUTE = 59,
            MAX_SECOND = 59;
}
