package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

import java.util.Arrays;

/**
 * The packet that is used to get the images by index
 */
@BinarySerializable
public class GetImagePacket extends Packet {

    @Serialize(offset = 0, itemCount = ImagePacket.IMAGES_COUNT)
    private byte[] imageIndexes;

    /**
     * Create a packet with indexes specified
     * @param indexes
     */
    public GetImagePacket(byte[] indexes) {
        assert(indexes.length < ImagePacket.IMAGES_COUNT);
        imageIndexes = new byte[ImagePacket.IMAGES_COUNT];
        Arrays.fill(imageIndexes, InvalidItemIndex.BYTE_VALUE);
        System.arraycopy(indexes, 0, imageIndexes, 0, indexes.length);
        setPacketType(PacketType.GET_IMAGE);
    }

    public byte[] getImageIndexes() {
        return imageIndexes;
    }
}
