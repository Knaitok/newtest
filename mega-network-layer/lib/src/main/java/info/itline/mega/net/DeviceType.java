package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.FromInteger;
import info.itline.binaryserializer.ToInteger;
import java.util.HashMap;
import java.util.Map;

@BinarySerializable(byteCount = 1)
public enum DeviceType {
    
    MEGA_BOARD                  (6);
    
    private DeviceType(int id) {
        this.id = (byte)id;
    }
    
    @ToInteger
    byte toInteger() {
        return id;
    }
    
    @FromInteger
    static DeviceType fromInteger(byte v) {
        return map.get(v);
    }
    
    private static final Map<Byte, DeviceType> map = new HashMap<>();
    static {
        for (DeviceType t: DeviceType.values()) {
            map.put(t.toInteger(), t);
        }
    }
    
    private final byte id;
}
