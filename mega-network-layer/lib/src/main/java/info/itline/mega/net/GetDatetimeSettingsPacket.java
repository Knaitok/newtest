package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class GetDatetimeSettingsPacket extends Packet {

    GetDatetimeSettingsPacket() {
        setPacketType(PacketType.GET_DATETIME_SETTINGS);
    }
}
