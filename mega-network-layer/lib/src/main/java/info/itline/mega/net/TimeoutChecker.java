package info.itline.mega.net;

import info.itline.mega.net.handlers.NettyRequestHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;

class TimeoutChecker extends TimerTask {

    private final Map<Integer, NettyRequestHandler> requests;
    private final int timeout;

    TimeoutChecker(Map<Integer, NettyRequestHandler> requests, int timeout) {
        this.requests = requests;
        this.timeout = timeout;
    }

    @Override
    public void run() {
        Map<Integer, NettyRequestHandler> requestsCopy = new HashMap<>(requests);
        long currentTimestamp = System.currentTimeMillis();
        requestsCopy.keySet().stream()
                .filter(key -> currentTimestamp - requestsCopy.get(key).getSentTimestamp() > timeout)
                .forEach(packetId -> {
                    requests.get(packetId).fireOnError("Timeout error");
                    requests.remove(packetId);
                });
    }
}