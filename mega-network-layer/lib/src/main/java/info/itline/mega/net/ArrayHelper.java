package info.itline.mega.net;

import java.lang.reflect.Array;

class ArrayHelper {

    ArrayHelper() {
    }
    
    static <T> T[] copyAndFill(T[] src, int dstLength, T defaultValue) {
        assert(src.length <= dstLength);
        T[] result = (T[]) Array.newInstance(src.getClass().getComponentType(), dstLength);
        System.arraycopy(src, 0, result, 0, src.length);
        for (int i = 0; i < dstLength - src.length; i++) {
            result[src.length + i] = defaultValue;
        }
        return result;
    }
}
