package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class PutTransientStringPacket extends TransientStringPacket {

    public PutTransientStringPacket(byte[] idx, byte[] profile, String[] strings) {
        super(idx, profile, strings);
        setPacketType(PacketType.PUT_TRANSIENT_STRING);
    }
}
