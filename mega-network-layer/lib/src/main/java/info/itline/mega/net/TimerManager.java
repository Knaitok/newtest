package info.itline.mega.net;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class TimerManager {

   private TimerManager() {
      Thread t = new Thread(() -> {
         while (true) {
            synchronized (TimerManager.this) {
               DeviceId id = getTargetDeviceId();
               if (id == null) {
                  continue;
               }
               Client.getInstance().getTimerConfig(id, indexArray,
                  p -> {
                     TimerConfig[] configs = p.getConfigs();
                     short[] indexes = p.getIds();
                     Map<Short, StateListener> listenersCopy = copyListeners();
                     for (int i = 0; i < indexes.length; i++) {
                        StateListener lst = listenersCopy.get(indexes[i]);
                        if (lst != null) {
                           lst.onStateUpdated(configs[i]);
                        }
                     }
                  }, null);
            }
            try {
               Thread.sleep(500);
            }
            catch (InterruptedException e) {

            }
         }
      });
      t.setDaemon(true);
      t.start();
   }

   private synchronized Map<Short, StateListener> copyListeners() {
      Map<Short, StateListener> result = new HashMap<>();
      result.putAll(listeners);
      return result;
   }

   public static synchronized TimerManager getInstance() {
      if (instance == null) {
         instance = new TimerManager();
      }
      return instance;
   }

   public static interface StateListener {

      void onStateUpdated(TimerConfig cfg);
   }

   public synchronized StateListener getStateListener(short idx) {
      return listeners.get(idx);
   }

   public synchronized void stopRegisteredTimers(
      Client.OnReceive<TimerConfigPacket> rcv, Client.OnError err) {
      if (masterTimerIdx == -1) {
         Client.getInstance().stopTimer(targetDevice.get(), indexArray, rcv, err);
      }
      else {
         Client.getInstance().pauseTimer(targetDevice.get(), masterTimerIdx, rcv, err);
      }
   }

   public synchronized void startRegisteredTimers(
      Client.OnReceive<TimerConfigPacket> rcv, Client.OnError err) {
      if (masterTimerIdx == -1) {
         Client.getInstance().startTimer(targetDevice.get(), indexArray, rcv, err);
      }
      else {
         Client.getInstance().startTimer(targetDevice.get(), masterTimerIdx, rcv, err);
      }
   }

   public synchronized void registerTimer(short idx, StateListener listener) {
      if (listener == null) {
         listeners.remove(idx);
      }
      else {
         listeners.put(idx, listener);
      }

      indexArray = makeIndexArray(listeners);
   }

   public synchronized void removeTimer(short idx) {
      registerTimer(idx, null);
   }

   private static short[] makeIndexArray(Map<Short, StateListener> listeners) {
      int i = 0;
      short[] result = new short[listeners.size()];
      for (short s : listeners.keySet()) {
         result[i++] = s;
      }
      return result;
   }

   public void setTargetDeviceId(DeviceId id) {
      targetDevice.set(id);
   }

   public DeviceId getTargetDeviceId() {
      return targetDevice.get();
   }

   public short getMasterTimerIdx() {
      return masterTimerIdx;
   }

   public void setMasterTimerIdx(short idx) {
      masterTimerIdx = idx;
   }

   private final AtomicReference<DeviceId> targetDevice = new AtomicReference<>();
   private short[] indexArray = new short[0];
   private final Map<Short, StateListener> listeners = new HashMap<>();
   private short masterTimerIdx = -1;
   private static TimerManager instance;
}
