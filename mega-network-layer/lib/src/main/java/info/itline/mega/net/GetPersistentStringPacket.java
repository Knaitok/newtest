package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
class GetPersistentStringPacket extends Packet {

    GetPersistentStringPacket(byte idx) {
        setPacketType(PacketType.GET_PERSISTENT_STRING);
        this.idx = idx;
    }

    public byte getIdx() {
        return idx;
    }
    
    @Serialize(offset = 0)
    private byte idx;
}
