package info.itline.mega.net.handlers;

import info.itline.mega.net.Packet;

public interface OnReceive<T extends Packet> {
    void onReceive(T packet);
}
