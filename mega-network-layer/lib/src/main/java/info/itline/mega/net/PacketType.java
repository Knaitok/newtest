package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.FromInteger;
import info.itline.binaryserializer.ToInteger;
import java.util.HashMap;
import java.util.Map;

@BinarySerializable(byteCount = 1)
enum PacketType {
    
    SEARCH                              (1),
    DEVICE_INFO                         (2),
    GET_DEVICE_INFO                     (3),
    
    GET_NETWORK_SETTINGS                (4),
    NETWORK_SETTINGS                    (5),
    PUT_NETWORK_SETTINGS                (6),
    
    GET_NVRAM_SETIINGS                  (7),
    NVRAM_SETTINGS                      (8),
    PUT_NVRAM_SETTINGS                  (9),
    
    GET_NVRAM_CONFIG                    (10),
    NVRAM_CONFIG                        (11),
    PUT_NVRAM_CONFIG                    (12),
    
    GET_DATETIME_SETTINGS               (13),
    DATETIME_SETTINGS                   (14),
    PUT_DATETIME_SETTINGS               (15),

    GET_PERSISTENT_STRING               (20),
    PERSISTENT_STRING                   (21),
    PUT_PERSISTENT_STRING               (22),

    GET_TRANSIENT_STRING                (23),
    TRANSIENT_STRING                    (24),
    PUT_TRANSIENT_STRING                (25),
    
    GET_CONST_STRING                    (26),
    CONST_STRING                        (27),
    
    GET_PERSISTENT_NUMBER               (28),
    PERSISTENT_NUMBER                   (29),
    PUT_PERSISTENT_NUMBER               (30),

    GET_TRANSIENT_NUMBER                (31),
    TRANSIENT_NUMBER                    (32),
    PUT_TRANSIENT_NUMBER                (33),
    
    GET_CONST_NUMBER                    (34),
    CONST_NUMBER                        (35),
    
    GET_MODE                            (50),
    MODE                                (51),
    PUT_MODE                            (52),
    
    GET_TIMER_CONFIG                    (36),
    TIMER_CONFIG                        (37),
    PUT_TIMER_CONFIG                    (38),

    GET_EE_CONSTANTS                    (39),
    EE_CONSTANTS                        (40),
    PUT_EE_CONSTANTS                    (41),

    GET_IMAGE                           (45),
    IMAGE                               (46),
    PUT_IMAGE                           (47);
    
    private PacketType(int value) {
        this.value = (byte)value;
    }
    
    @ToInteger
    byte toInteger() {
        return value;
    }
    
    @FromInteger
    static PacketType fromInteger(byte b) {
        return map.get(b);
    }
    
    private final static Map<Byte, PacketType> map = new HashMap<>();
    static {
        for (PacketType t: PacketType.values()) {
            map.put(t.toInteger(), t);
        }
    }
    
    private final byte value;
}
