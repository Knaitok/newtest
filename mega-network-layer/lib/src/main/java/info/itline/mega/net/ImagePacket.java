package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A packet that describes the image. Images are stored
 * in the location for the strings
 */
@BinarySerializable
public class ImagePacket extends Packet {

    public static final int IMAGES_COUNT = 8;

    public static final int IMAGE_LENGTH = 128;

    @Serialize(offset = 0, itemCount = IMAGES_COUNT)
    private byte[] indexes;

    @Serialize(offset = 1, itemCount = IMAGES_COUNT * IMAGE_LENGTH)
    private byte[] imageData;

    /*
     * Initialize the buffers
     */
    {
        indexes = new byte[IMAGES_COUNT];
        imageData = new byte[IMAGES_COUNT * IMAGE_LENGTH];
        Arrays.fill(indexes, InvalidItemIndex.BYTE_VALUE);
        Arrays.fill(imageData, (byte) 0);
        setPacketType(PacketType.IMAGE);
    }

    /**
     * Default constructor to place data into the storage
     */
    public ImagePacket() {

    }

    /**
     * Create an ImagePacket and fill it with the data
     * @param newIndexes
     * @param images
     */
    public ImagePacket(byte[] newIndexes, List<byte[]> images) {
        assert(newIndexes.length == images.size());

        System.arraycopy(newIndexes, 0, indexes, 0, newIndexes.length);
        for(int i = 0; i < images.size(); i++) {
            byte[] data = images.get(i);
            assert(data.length <= IMAGE_LENGTH);
            System.arraycopy(data, 0, imageData, i * IMAGE_LENGTH, data.length);
        }
    }

    /**
     * Get indexes of the images in the packet
     * @return
     */
    public byte[] getIndexes() {
        return indexes;
    }

    /**
     * Get a set of images
     * @return
     */
    public List<byte[]> getImages() {
        ArrayList<byte[]> images = new ArrayList<>();
        for(int i = 0; i < indexes.length; i++) {
            if (indexes[0] == InvalidItemIndex.BYTE_VALUE)
                break;
            byte[] image = new byte[IMAGE_LENGTH];
            Arrays.fill(image, (byte) 0);
            System.arraycopy(imageData, i * IMAGE_LENGTH, image, 0, IMAGE_LENGTH);
            images.add(image);
        }
        return images;
    }
}
