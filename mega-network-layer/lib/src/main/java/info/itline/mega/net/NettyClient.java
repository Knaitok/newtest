package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializer;
import info.itline.mega.net.handlers.NettyRequestHandler;
import info.itline.mega.net.handlers.OnError;
import info.itline.mega.net.handlers.OnReceive;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.nio.NioDatagramChannel;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class NettyClient {
    private static final int SERVER_SOCKET_PORT = 5001;
    private static final int MAX_PACKET_SIZE = 2048;
    private static final int DEFAULT_TIMEOUT = 500;

    private int packetId = 0;

    private final DeviceId device;
    private final Map<Integer, NettyRequestHandler> requests = new HashMap<>();
    private Channel channel;
    private EventLoopGroup workerGroup;

    private BinarySerializer serializer = new BinarySerializer("windows-1251");

    public NettyClient(DeviceId device) {
        this(device, DEFAULT_TIMEOUT);
    }

    public NettyClient(DeviceId device, int timeout) {
        this.device = device;
        workerGroup = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(workerGroup);
        bootstrap.channel(NioDatagramChannel.class).handler(new NettyClientHandler(requests))
                .option(ChannelOption.SO_BROADCAST, true);
        try {
            channel = bootstrap.bind(0).sync().channel();
        } catch (InterruptedException e) {
            workerGroup.shutdownGracefully();
            throw new RuntimeException(e);
        }
        workerGroup.scheduleAtFixedRate(new TimeoutChecker(requests, timeout), 0, timeout, TimeUnit.MILLISECONDS);
    }

    public void getDeviceInfo(OnReceive<DeviceInfoPacket> receiveHandler, OnError errorHandler) {
        send(new GetDeviceInfoPacket(), DeviceInfoPacket.class, receiveHandler, errorHandler);
    }

    public void getDateTime(OnReceive<DateTimeSettingsPacket> receiveHandler, OnError errorHandler) {
        send(new GetDatetimeSettingsPacket(), DateTimeSettingsPacket.class, receiveHandler, errorHandler);
    }

    public void synchronizeDateTime(OnReceive<DateTimeSettingsPacket> receiveHandler, OnError errorHandler) {
        send(new PutDateTimeSettings(), DateTimeSettingsPacket.class, receiveHandler, errorHandler);
    }

    public void getMode(OnReceive<ModePacket> receiveHandler, OnError errorHandler) {
        send(new GetModePacket(), ModePacket.class, receiveHandler, errorHandler);
    }

    public void setMode(int mode, OnReceive<ModePacket> receiveHandler, OnError errorHandler) {
        send(new PutModePacket(mode), ModePacket.class, receiveHandler, errorHandler);
    }

    public void getEEConstants(OnReceive<EEConstantsPacket> receiveHandler, OnError errorHandler) {
        send(new GetEEConstantsPacket(), EEConstantsPacket.class, receiveHandler, errorHandler);
    }

    public void setEEConstants(EEConstantsPacket constants,
                               OnReceive<EEConstantsPacket> receiveHandler, OnError errorHandler) {
        send(new PutEEConstantsPacket(constants), EEConstantsPacket.class, receiveHandler, errorHandler);
    }

    public void getNetworkSetings(OnReceive<NetworkSettingsPacket> receiveHandler, OnError errorHandler) {
        send(new GetNetworkSettingsPacket(), NetworkSettingsPacket.class, receiveHandler, errorHandler);
    }

    public void setNetworkSettings(IpAddress ip, IpAddress mask, IpAddress gateway, String name,
                                   OnReceive<NetworkSettingsPacket> receiveHandler, OnError errorHandler) {
        send(new PutNetworkSettingsPacket(name, ip, mask, gateway), NetworkSettingsPacket.class,
                receiveHandler, errorHandler);
    }

    public void getNvramConfig(OnReceive<NvramConfigPacket> receiveHandler, OnError errorHandler) {
        send(new GetNvramConfigPacket(), NvramConfigPacket.class, receiveHandler, errorHandler);
    }

    public void setNvramConfig(int safe, short min, short max, int watchdogPeriod, boolean autoBrightness,
                               OnReceive<NvramConfigPacket> receivaHandler, OnError errorHandler) {
        send(new PutNvramConfigPacket(safe, min, max, watchdogPeriod, autoBrightness ? 1 : 0),
                NvramConfigPacket.class, receivaHandler, errorHandler);
    }

    public void getNvramSettings(OnReceive<NvramSettingsPacket> receiveHandler, OnError errorHandler) {
        send(new GetNvramSettingsPacket(), NvramSettingsPacket.class, receiveHandler, errorHandler);
    }

    public void setNvramSettings(int brightness,
                                 OnReceive<NvramSettingsPacket> receiveHandler, OnError errorHandler) {
        send(new PutNvramSettingsPacket(brightness), NvramSettingsPacket.class, receiveHandler, errorHandler);
    }

    public void getPersistentString(byte index,
                                    OnReceive<PersistentStringPacket> receivehandler, OnError errorHandler) {
        send(new GetPersistentStringPacket(index), PersistentStringPacket.class, receivehandler, errorHandler);
    }

    public void setPersistentString(byte index, byte profile, String string,
                                    OnReceive<PersistentStringPacket> receivehandler, OnError errorHandler) {
        send(new PutPersistentStringPacket(index, profile, string), PersistentStringPacket.class,
                receivehandler, errorHandler);
    }

    public void getTransientString(byte[] indexes,
                                   OnReceive<TransientStringPacket> receivehandler, OnError errorHandler) {
        send(new GetTransientStringPacket(indexes), TransientStringPacket.class, receivehandler, errorHandler);
    }

    public void setTransientString(byte[] indexes, byte[] profiles, String[] strings,
                                   OnReceive<TransientStringPacket> receivehandler, OnError errorHandler) {
        send(new PutTransientStringPacket(indexes, profiles, strings), TransientStringPacket.class,
                receivehandler, errorHandler);
    }

    public void getPersistentNumber(short[] indexes,
                                    OnReceive<PersistentNumberPacket> receivehandler, OnError errorHandler) {
        send(new GetPersistentNumberPacket(indexes), PersistentNumberPacket.class, receivehandler, errorHandler);
    }

    public void setPersistentNumber(short[] indexes, int[] numbers,
                                    OnReceive<PersistentNumberPacket> receivehandler, OnError errorHandler) {
        send(new PutPersistentNumberPacket(indexes, numbers), PersistentNumberPacket.class,
                receivehandler, errorHandler);
    }

    public void getTransientNumber(short[] indexes,
                                   OnReceive<TransientNumberPacket> receivehandler, OnError errorHandler) {
        send(new GetTransientNumberPacket(indexes), TransientNumberPacket.class, receivehandler, errorHandler);
    }

    public void setTransientNumber(short[] indexes, int[] numbers,
                                   OnReceive<TransientNumberPacket> receivehandler, OnError errorHandler) {
        send(new PutTransientNumberPacket(indexes, numbers), TransientNumberPacket.class,
                receivehandler, errorHandler);
    }

    public void getImages(byte[] indexes, OnReceive<ImagePacket> receiveHandler,
                          OnError errorHandler) {
        send(new GetImagePacket(indexes), ImagePacket.class, receiveHandler, errorHandler);
    }

    public void setImages(byte[] indexes, List<byte[]> images,
                          OnReceive<ImagePacket> receiveHandler, OnError errorHandler) {
        send(new PutImagePacket(indexes, images), ImagePacket.class, receiveHandler, errorHandler);
    }

    public void getTimerConfig(short[] indexes,
                               OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        send(new GetTimerConfigPacket(indexes), TimerConfigPacket.class, receiveHandler, errorHandler);
    }

    public void getTimerConfig(short index,
                               OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        getTimerConfig(new short[]{index}, receiveHandler, errorHandler);
    }

    public void setTimerConfig(short[] indexes, TimerConfig[] configs,
                               OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        send(new PutTimerConfigPacket(indexes, configs), TimerConfigPacket.class, receiveHandler, errorHandler);
    }

    public void setTimerConfig(short index, TimerConfig config,
                               OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerConfig(new short[]{index}, new TimerConfig[]{config}, receiveHandler, errorHandler);
    }

    public void setTimerStartAndStopValues(short index, int startValue, int stopValue,
                                           OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerConfig(index, new TimerConfig(0, startValue * 100, stopValue * 100, (short) 0, (byte) 0,
                        (byte) (TimerConfigField.START_VALUE.getValue() | TimerConfigField.STOP_VALUE.getValue())),
                receiveHandler, errorHandler);
    }

    public void setTimerCurrentValue(short index, int value,
                                     OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerConfig(index, new TimerConfig(value * 100, 0, 0, (short) 0, (byte) 0,
                        TimerConfigField.CURRENT_VALUE.getValue()), receiveHandler, errorHandler);
    }

    private void setTimerState(short index, byte state,
                               OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerConfig(index, new TimerConfig(0, 0, 0, (short) 0, state, TimerConfigField.STATE.getValue()),
                receiveHandler, errorHandler);
    }

    private void setTimerState(short[] index, byte state,
                               OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        TimerConfig timerConfig = new TimerConfig(0, 0, 0, (short) 0, state, TimerConfigField.STATE.getValue());
        TimerConfig[] timerConfigs = new TimerConfig[index.length];
        for (int i = 0; i < index.length; i++) {
            timerConfigs[i] = timerConfig;
        }
        setTimerConfig(index, timerConfigs, receiveHandler, errorHandler);
    }

    private TimerConfig[] copyTimerConfig(TimerConfig timerConfig, int count) {
        TimerConfig[] result = new TimerConfig[count];
        for (int i = 0; i < count; i++) {
            result[i] = timerConfig;
        }
        return result;
    }

    public void startTimer(short[] indexes,
                           OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerState(indexes, TimerState.RUNNING.getValue(), receiveHandler, errorHandler);
    }

    public void stopTimer(short[] indexes,
                          OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerState(indexes, TimerState.STOPPED.getValue(), receiveHandler, errorHandler);
    }

    public void resetTimer(short[] indexes, int currentValue,
                           OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerConfig(indexes,
                copyTimerConfig(new TimerConfig(currentValue, 0, 0, (short) 0, TimerState.STOPPED.getValue(),
                        (byte) (TimerConfigField.CURRENT_VALUE.getValue() | TimerConfigField.STATE.getValue())),
                        indexes.length),
                receiveHandler, errorHandler);
    }

    public void startTimer(short index,
                           OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerState(index, TimerState.RUNNING.getValue(), receiveHandler, errorHandler);
    }

    public void pauseTimer(short index,
                           OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerState(index, TimerState.STOPPED.getValue(), receiveHandler, errorHandler);
    }

    public void resetTimer(short index, int currentValue,
                           OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerConfig(index, new TimerConfig(currentValue, 0, 0, (short) 0, TimerState.STOPPED.getValue(),
                (byte) (TimerConfigField.CURRENT_VALUE.getValue() | TimerConfigField.STATE.getValue())),
                receiveHandler, errorHandler);
    }

    public void triggerSiren(int timerIndex) {
        TimerConfig cfg = new TimerConfig(0, 0, 1,
                TimerConfig.makeTimerFlags(false, 0, TimerConfig.ENABLE_SIREN),
                TimerState.RUNNING.getValue(), TimerConfigField.ALL.getValue());
        setTimerConfig((byte) timerIndex, cfg, null, null);
    }

    public void send(Packet packet, Class<? extends Packet> packetClass,
                     OnReceive<? extends Packet> onReceive, OnError onError) {
        try {
            packet.setPacketId(packetId);
            packet.setDeviceType(device.getType());
            packet.setMacAddress(device.getMacAddress());
            ByteBuf byteBuf = channel.alloc().buffer(MAX_PACKET_SIZE);
            byteBuf.writeBytes(serializer.serialize(packet));
            requests.put(packetId++, new NettyRequestHandler(packetClass, onReceive, onError));
            DatagramPacket datagramPacket = new DatagramPacket(byteBuf,
                    new InetSocketAddress(device.getIpAddress().convertToInetAddress(), SERVER_SOCKET_PORT));
            channel.writeAndFlush(datagramPacket).sync();
        } catch (InterruptedException | IOException e) {
            requests.get(packet.getId()).fireOnError(e.getMessage());
            requests.remove(packet.getId());
        }
    }

    public void closeChannel() {
        channel.closeFuture();
        workerGroup.shutdownGracefully();
    }

    public static void main(String... args) {
        NettyClient nettyClient = new NettyClient(new DeviceId(DeviceType.MEGA_BOARD, IpAddress.parse("10.7.202.20"), MacAddress.parse("00:40:00:00:02:b8"), ""));
        nettyClient.getTransientString(new byte[]{0, 1, 2, 3, 4, 5}, packet -> {
            for (int i = 0; i < packet.getStrings().length; i++) {
                System.out.print(packet.getStrings()[i] + " ");
            }
            System.out.println();
        }, null);
        nettyClient.getTransientString(new byte[]{5, 4, 3, 2, 1, 0}, packet -> {
            for (int i = 0; i < packet.getStrings().length; i++) {
                System.out.print(packet.getStrings()[i] + " ");
            }
            System.out.println();
        }, null);
        nettyClient.closeChannel();
    }
}
