package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
public class Packet {
    
    public final MacAddress getMacAddress() {
        return macAddress;
    }

    public final void setMacAddress(MacAddress macAddress) {
        this.macAddress = macAddress;
    }

    final PacketType getPacketType() {
        return packetType;
    }

    final void setPacketType(PacketType packetType) {
        this.packetType = packetType;
    }

    final DeviceType getDeviceType() {
        return deviceType;
    }

    final void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    final int getId() {
        return id;
    }
    
    final void setPacketId(int packetId) {
        this.id = packetId;
    }

    final public boolean isWaitForMultipleResponses() {
        return waitForMultipleResponses;
    }

    protected final void setWaitForMultipleResponses(boolean waitForMultipleResponses) {
        this.waitForMultipleResponses = waitForMultipleResponses;
    }
    
    void checkType(PacketType expected) 
            throws InvalidPacketException {
        if (expected != getPacketType()) {
            throw new InvalidPacketException("Expected " + expected + ", got " + getPacketType());
        }
    }

    @Override
    public String toString() {
        return "Packet{" + 
                "macAddress=" + macAddress + 
                ", packetType=" + packetType + 
                ", deviceType=" + deviceType + 
                ", id=" + id + '}';
    }
    
    @Serialize(offset = 0)
    private MacAddress macAddress;
    @Serialize(offset = 1)
    private PacketType packetType;
    @Serialize(offset = 2)
    private DeviceType deviceType;
    @Serialize(offset = 3)
    private int id;
    
    private boolean waitForMultipleResponses;
}
