package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;
import java.io.Serializable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@BinarySerializable
public class IpAddress implements Serializable {
    
    private IpAddress() {
        bytes = null;
    }
    
    IpAddress(byte[] bytes) {
        this.bytes = new byte[SIZE];
        System.arraycopy(bytes, 0, this.bytes, 0, SIZE);
    }
    
    InetAddress convertToInetAddress() {
        try {
            return Inet4Address.getByAddress(bytes);
        }
        catch (UnknownHostException ignore) {
            throw new RuntimeException("No such host");
        }
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Arrays.hashCode(this.bytes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IpAddress other = (IpAddress) obj;
        if (!Arrays.equals(this.bytes, other.bytes)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return FormatHelper.formatAddress(bytes, "%d", ".");
    }
    
    public static boolean isAddress(String s) {
        Matcher m = IP_PATTERN.matcher(s);
        if (!m.matches())
            return false;
        for (int i = 0; i < SIZE; i++) {
            if (Integer.parseInt(m.group(i + 1)) > 0xFF) {
                return false;
            }
        }
        return true;
    }
    
    public static IpAddress parse(String s) throws IllegalArgumentException {
        Matcher m = IP_PATTERN.matcher(s);
        IllegalArgumentException exc = 
                new IllegalArgumentException("Строка не является IP-адресом");
        if (!m.matches() || m.groupCount() != SIZE) {
            throw exc;
        }
        byte[] buf = new byte[SIZE];
        for (int i = 0; i < m.groupCount(); i++) {
            int v = Integer.parseInt(m.group(i + 1));
            if (v > 255)
                throw new IllegalArgumentException(
                        "Значения байтов IP-адреса не должны быть больше 255");
            buf[i] = (byte) v;
        }
        return new IpAddress(buf);
    }
    
    @Serialize(offset = 1, itemCount = SIZE)
    private final byte[] bytes;
    
    public static final int SIZE = 4;
    public static final IpAddress broadcastAddress;
    
    private static final Pattern IP_PATTERN;
    static {
        String b = "\\s*([0-9]{1,3})\\s*";
        String s = ".";
        IP_PATTERN = Pattern.compile(
                "^" + b + s + b + s + b + s + b +"$");
        broadcastAddress = IpAddress.parse("255.255.255.255");
    }
    
    private static final long serialVersionUID = 1;
}
