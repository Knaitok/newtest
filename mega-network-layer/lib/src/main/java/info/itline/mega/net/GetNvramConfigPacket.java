package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class GetNvramConfigPacket extends Packet {

    public GetNvramConfigPacket() {
        setPacketType(PacketType.GET_NVRAM_CONFIG);
    }
}
