package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
public class PersistentStringPacket extends Packet {

    PersistentStringPacket(byte idx, byte profile, String string) {
        this.idx = idx;
        this.profile = profile;
        this.string = string;
        setPacketType(PacketType.PERSISTENT_STRING);
    }

    private PersistentStringPacket() {
        this((byte)0, (byte)0, null);
    }

    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.PERSISTENT_STRING);
    }
    
    public byte getIdx() {
        return idx;
    }

    public byte getProfile() {
        return profile;
    }

    public String getString() {
        return string;
    }

    @Serialize(offset = 0)
    private final byte idx;
    @Serialize(offset = 1)
    private final byte profile;
    @Serialize(offset = 2, maxLength = 1024)
    private final String string;
}
