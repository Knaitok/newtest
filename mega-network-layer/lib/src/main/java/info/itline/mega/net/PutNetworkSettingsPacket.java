package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class PutNetworkSettingsPacket extends NetworkSettingsPacket {

    public PutNetworkSettingsPacket(String name, IpAddress ipAddress, IpAddress netmask, IpAddress gateway) {
        super(name, ipAddress, netmask, gateway);
        setPacketType(PacketType.PUT_NETWORK_SETTINGS);
    }
}
