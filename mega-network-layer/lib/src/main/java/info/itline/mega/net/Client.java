package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializer;
import info.itline.binaryserializer.PostCreateException;
import java.io.IOException;
import java.io.NotSerializableException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javafx.application.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static info.itline.mega.net.TimerConfigField.*;
import static info.itline.mega.net.TimerState.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.GregorianCalendar;

public class Client {

    private Client() throws IOException {
        broadcastAddress = InetAddress.getByName("255.255.255.255");
        socket = new DatagramSocket();
        socket.setSoTimeout(SOCKET_TIMEOUT);
        serializer = new BinarySerializer("windows-1251");
        sender = new Sender();
        receiver = new Receiver();

        startDaemonThread(sender);
        startDaemonThread(receiver);
    }

    private static void startDaemonThread(Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        t.start();
    }

    public static synchronized  void initialize() throws IOException {
        if(instance == null) {
            instance = new Client();
        }
    }

    public synchronized void useInternalWorkerThread() {
        workerThreadRunnable = new WorkerThreadRunnable();
        startDaemonThread(workerThreadRunnable);
        useInternalWorkerThread = true;
    }
    
    public static synchronized Client getInstance() {
        assert (instance != null);
        return instance;
    }

    public void search(DeviceType deviceType,
            OnReceive<DeviceInfoPacket> receiveHandler,
            OnError errorHandler, OnDone doneHandler) {
        DeviceId id = new DeviceId(deviceType,
                IpAddress.broadcastAddress, MacAddress.broadcastAddress, "");
        Packet p = new SearchRequestPacket();
        send(id, p, DeviceInfoPacket.class, receiveHandler, errorHandler, doneHandler);
    }

    public void getDeviceInfo(DeviceId id,
            OnReceive<DeviceInfoPacket> receiveHandler,
            OnError errorHandler) {
        send(id, new GetDeviceInfoPacket(), DeviceInfoPacket.class,
                receiveHandler, errorHandler, null);
    }

    public void getDateTime(DeviceId id,
            OnReceive<DateTimeSettingsPacket> receiveHandler,
            OnError errorHandler) {
        send(id, new GetDatetimeSettingsPacket(),
                DateTimeSettingsPacket.class, receiveHandler, errorHandler, null);
    }

    public void setDateTime(DeviceId id, GregorianCalendar calendar,
                                    OnReceive<DateTimeSettingsPacket> receiveHandler,
                                    OnError errorHandler) {
        send(id, new PutDateTimeSettings(calendar), DateTimeSettingsPacket.class,
                receiveHandler, errorHandler, null);
    }

    public void synchronizeDateTime(DeviceId id,
            OnReceive<DateTimeSettingsPacket> receiveHandler,
            OnError errorHandler) {
        send(id, new PutDateTimeSettings(), DateTimeSettingsPacket.class,
                receiveHandler, errorHandler, null);
    }

    public void getMode(DeviceId id, OnReceive<ModePacket> receiveHandler,
            OnError errorHandler) {
        send(id, new GetModePacket(), ModePacket.class, receiveHandler,
                errorHandler, null);
    }

    public void setMode(DeviceId id, int mode, OnReceive<ModePacket> receiveHandler,
            OnError errorHandler) {
        send(id, new PutModePacket(mode), ModePacket.class, receiveHandler,
                errorHandler, null);
    }

    public void getEEConstants(DeviceId id, OnReceive<EEConstantsPacket> receiveHandler,
                               OnError errorHandler) {
        send(id, new GetEEConstantsPacket(), EEConstantsPacket.class, receiveHandler,
                errorHandler, null);
    }

    public void setEEConstants(DeviceId id, EEConstantsPacket constants,
                               OnReceive<EEConstantsPacket> receiveHandler, OnError errorHandler) {
        send(id, new PutEEConstantsPacket(constants), EEConstantsPacket.class,
                receiveHandler, errorHandler, null);
    }

    public void getNetworkSetings(DeviceId id,
            OnReceive<NetworkSettingsPacket> receiveHandler,
            OnError errorHandler) {
        send(id, new GetNetworkSettingsPacket(), NetworkSettingsPacket.class,
                receiveHandler, errorHandler, null);
    }

    public void setNetworkSettings(DeviceId id,
            IpAddress ip, IpAddress mask, IpAddress gw, String name,
            OnReceive<NetworkSettingsPacket> receiveHandler, OnError errorHandler) {
        send(id, new PutNetworkSettingsPacket(name, ip, mask, gw),
                NetworkSettingsPacket.class, receiveHandler, errorHandler, null);
    }

    public void getNvramConfig(DeviceId id,
            OnReceive<NvramConfigPacket> receiveHandler,
            OnError errorHandler) {
        send(id, new GetNvramConfigPacket(),
                NvramConfigPacket.class, receiveHandler, errorHandler, null);
    }

    public void setNvramConfig(DeviceId id, int safe, short min, short max,
            int watchdogPeriod, boolean autoBrightness,
            OnReceive<NvramConfigPacket> receivaHandler, OnError errorHandler) {
        send(id, new PutNvramConfigPacket(safe, min, max, watchdogPeriod,
                autoBrightness ? 1 : 0),
                NvramConfigPacket.class, receivaHandler, errorHandler, null);
    }

    public void getNvramSettings(DeviceId id, OnReceive<NvramSettingsPacket> receiveHandler,
            OnError errorHandler) {
        send(id, new GetNvramSettingsPacket(), NvramSettingsPacket.class,
                receiveHandler, errorHandler, null);
    }

    public void setNvramSettings(DeviceId id, int brightness,
            OnReceive<NvramSettingsPacket> receiveHandler, OnError errorHandler) {
        send(id, new PutNvramSettingsPacket(brightness), NvramSettingsPacket.class,
                receiveHandler, errorHandler, null);
    }

    public void getPersistentString(DeviceId id, byte idx,
            OnReceive<PersistentStringPacket> receivehandler, OnError errorHandler) {
        send(id, new GetPersistentStringPacket(idx), PersistentStringPacket.class,
                receivehandler, errorHandler, null);
    }

    public void setPersistentString(DeviceId id, byte idx, byte profile, String string,
            OnReceive<PersistentStringPacket> receivehandler, OnError errorHandler) {
        send(id, new PutPersistentStringPacket(idx, profile, string),
                PersistentStringPacket.class, receivehandler, errorHandler, null);
    }

    public void getTransientString(DeviceId id, byte[] idx,
            OnReceive<TransientStringPacket> receivehandler, OnError errorHandler) {
        send(id, new GetTransientStringPacket(idx), TransientStringPacket.class,
                receivehandler, errorHandler, null);
    }

    public void setTransientString(DeviceId id, byte[] idx, byte[] profile, String[] string,
            OnReceive<TransientStringPacket> receivehandler, OnError errorHandler) {
        send(id, new PutTransientStringPacket(idx, profile, string),
                TransientStringPacket.class, receivehandler, errorHandler, null);
    }

    public void getPersistentNumber(DeviceId id, short[] idx,
            OnReceive<PersistentNumberPacket> receivehandler, OnError errorHandler) {
        send(id, new GetPersistentNumberPacket(idx), PersistentNumberPacket.class,
                receivehandler, errorHandler, null);
    }

    public void setPersistentNumber(DeviceId id, short[] idx, int[] data,
            OnReceive<PersistentNumberPacket> receivehandler, OnError errorHandler) {
        send(id, new PutPersistentNumberPacket(idx, data),
                PersistentNumberPacket.class, receivehandler, errorHandler, null);
    }

    public void getTransientNumber(DeviceId id, short[] idx,
            OnReceive<TransientNumberPacket> receivehandler, OnError errorHandler) {
        send(id, new GetTransientNumberPacket(idx), TransientNumberPacket.class,
                receivehandler, errorHandler, null);
    }

    public void setTransientNumber(DeviceId id, short[] idx, int[] data,
            OnReceive<TransientNumberPacket> receivehandler, OnError errorHandler) {
        send(id, new PutTransientNumberPacket(idx, data),
                TransientNumberPacket.class, receivehandler, errorHandler, null);
    }

    public void getImages(DeviceId id, byte[] indexes, OnReceive<ImagePacket> receiveHandler,
                          OnError errorHandler) {
        send(id, new GetImagePacket(indexes), ImagePacket.class, receiveHandler,
                errorHandler, null);
    }

    public void setImages(DeviceId id, byte[] indexes, List<byte[]> images,
                          OnReceive<ImagePacket> receiveHandler, OnError errorHandler) {
        send(id, new PutImagePacket(indexes, images), ImagePacket.class,
                receiveHandler, errorHandler, null);
    }

    public void getTimerConfig(DeviceId id, short[] idx,
            OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        send(id, new GetTimerConfigPacket(idx), TimerConfigPacket.class,
                receiveHandler, errorHandler, null);
    }

    public void getTimerConfig(DeviceId id, short idx,
            OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        getTimerConfig(id, new short[]{idx}, receiveHandler, errorHandler);
    }

    public void setTimerConfig(DeviceId id, short[] idx, TimerConfig[] configs,
            OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        send(id, new PutTimerConfigPacket(idx, configs), TimerConfigPacket.class,
                receiveHandler, errorHandler, null);
    }

    public void setTimerConfig(DeviceId id, short idx, TimerConfig config,
            OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerConfig(id, new short[]{idx}, new TimerConfig[]{config},
                receiveHandler, errorHandler);
    }

    public void setTimerStartAndStopValues(DeviceId id, short idx, int startValue, int stopValue,
            OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerConfig(id, idx, new TimerConfig(0, startValue * 100, stopValue * 100, (short) 0, (byte) 0,
                (byte) (START_VALUE.getValue() | STOP_VALUE.getValue())),
                receiveHandler, errorHandler);
    }

    public void setTimerCurrentValue(DeviceId id, short idx, int value,
            OnReceive<TimerConfigPacket> rcv, OnError err) {
        setTimerConfig(id, idx, new TimerConfig(value * 100, 0, 0, (short) 0, (byte) 0,
                (byte) (CURRENT_VALUE.getValue())),
                rcv, err);
    }

    private void setTimerState(DeviceId id, short idx, byte state,
            OnReceive<TimerConfigPacket> recv, OnError err) {
        setTimerConfig(id, idx, new TimerConfig(0, 0, 0, (short) 0, state,
                (byte) (STATE.getValue())), recv, err);
    }

    private void setTimerState(DeviceId id, short[] idx, byte state,
            OnReceive<TimerConfigPacket> rcv, OnError err) {
        TimerConfig c = new TimerConfig(0, 0, 0, (short) 0, state, STATE.getValue());
        TimerConfig[] cfgs = new TimerConfig[idx.length];
        for (int i = 0; i < idx.length; i++) {
            cfgs[i] = c;
        }
        setTimerConfig(id, idx, cfgs, rcv, err);
    }

    private TimerConfig[] copyTimerConfig(TimerConfig c, int count) {
        TimerConfig[] result = new TimerConfig[count];
        for (int i = 0; i < count; i++) {
            result[i] = c;
        }
        return result;
    }

    public void startTimer(DeviceId id, short[] idx,
            OnReceive<TimerConfigPacket> rcv, OnError err) {
        setTimerState(id, idx, RUNNING.getValue(), rcv, err);
    }

    public void stopTimer(DeviceId id, short[] idx,
            OnReceive<TimerConfigPacket> rcv, OnError err) {
        setTimerState(id, idx, STOPPED.getValue(), rcv, err);
    }

    public void resetTimer(DeviceId id, short[] idx, int currentValue,
            OnReceive<TimerConfigPacket> rcv, OnError err) {
        setTimerConfig(id, idx,
                copyTimerConfig(
                        new TimerConfig(currentValue, 0, 0, (short) 0, STOPPED.getValue(),
                                (byte) (CURRENT_VALUE.getValue() | STATE.getValue())), idx.length),
                rcv, err);
    }

    public void startTimer(DeviceId id, short idx,
            OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerState(id, idx, RUNNING.getValue(), receiveHandler, errorHandler);
    }

    public void pauseTimer(DeviceId id, short idx,
            OnReceive<TimerConfigPacket> receiveHandler, OnError errorHandler) {
        setTimerState(id, idx, STOPPED.getValue(), receiveHandler, errorHandler);
    }

    public void resetTimer(DeviceId id, short idx, int currentValue,
            OnReceive<TimerConfigPacket> recv, OnError err) {
        setTimerConfig(id, idx, new TimerConfig(currentValue, 0, 0, (short) 0, STOPPED.getValue(),
                (byte) (CURRENT_VALUE.getValue() | STATE.getValue())), recv, err);
    }

    public void triggerSiren(DeviceId id, int timerIdx) {
        TimerConfig cfg = new TimerConfig(0, 0, 1,
                TimerConfig.makeTimerFlags(false, 0, TimerConfig.ENABLE_SIREN),
                TimerState.RUNNING.getValue(), TimerConfigField.ALL.getValue());
        Client.getInstance().setTimerConfig(id, (byte) timerIdx, cfg, null, null);
    }

    private synchronized void send(DeviceId device, Packet packet, Class<? extends Packet> responseClass,
            OnReceive onReceiveCallback, OnError onErrorCallback, OnDone onDoneCallback) {
        Request r = new Request(device, packet, responseClass, onReceiveCallback, onErrorCallback, onDoneCallback);
        sender.putRequest(r);
    }

    public static interface OnReceive<T extends Packet> {

        void handle(T p);
    }

    public static interface OnError {

        void handle(ErrorCode e, String message);
    }

    public static interface OnDone {

        void handle();
    }

    public static enum ErrorCode {

        TIMEOUT("Нет ответа от контроллера"),
        WRONG_PASSWORD("Неправильный пароль"),
        IO_EXCEPTION("Ошибка при работе с сетью"),
        WRONG_PACKET("Неправильный формат пакета");

        private ErrorCode(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        private final String description;
    }

    public boolean isSendBroadcastPackets() {
        return sendBroadcast;
    }

    public void setSendBroadcastPackets(boolean v) {
        this.sendBroadcast = v;
    }

    private class Sender implements Runnable {

        @Override
        public void run() {
            while (true) {
                try {
                    Request r = queue.take();
                    try {
                        r.packet.setPacketId(nextPacketId++);
                        r.packet.setDeviceType(r.deviceId.getType());
                        r.packet.setMacAddress(r.deviceId.getMacAddress());

                        buf.setData(serializer.serialize(r.packet));
                        buf.setAddress(sendBroadcast
                                ? broadcastAddress
                                : r.deviceId.getIpAddress().convertToInetAddress());
                        buf.setPort(SERVER_SOCKET_PORT);

                        r.sentTimestamp = System.currentTimeMillis();
                        receiver.putRequest(r);
                        socket.send(buf);

                        Thread.sleep(100);
                    }
                    catch (NotSerializableException e) {
                        logger.warn("Request object isn't serializable");
                        throw new RuntimeException(e);
                    }
                    catch (IOException e) {
                        logger.warn("Unable to send packet");
                    }
                }
                catch (InterruptedException e) {

                }
            }
        }

        void putRequest(Request r) {
            queue.add(r);
        }

        private final BlockingQueue<Request> queue = new LinkedBlockingQueue<>();
        private final DatagramPacket buf = new DatagramPacket(
                new byte[MAX_PACKET_SIZE], MAX_PACKET_SIZE);

        private int nextPacketId = 0;
    }

    private class Receiver implements Runnable {

        @Override
        public void run() {
            while (true) {
                Packet response = null;
                try {
                    socket.receive(buf);
                    response = serializer.deserialize(Packet.class, buf.getData());
                }
                catch (SocketTimeoutException e) {

                }
                catch (IOException e) {
                    logger.warn("Unable to receive packet");
                }

                boolean packetDispathed = false;
                synchronized (this) {
                    for (Iterator<Request> i = queue.iterator(); i.hasNext();) {
                        Request r = i.next();

                        if (response != null
                                && r.packet.getId() == response.getId()
                                && (response.getMacAddress().equals(r.deviceId.getMacAddress())
                                || r.deviceId.getMacAddress().equals(MacAddress.broadcastAddress))) {
                            packetDispathed = true;
                            try {
                                Packet p = serializer.deserialize(r.responseClass, buf.getData());
                                r.fireOnReceive(p);
                            }
                            catch (PostCreateException | IOException e) {
                                Throwable c = e instanceof PostCreateException ? e.getCause() : e;
                                logger.info("Unable to deserialize packet");
                                r.fireOnError(ErrorCode.WRONG_PACKET, e.getMessage());
                            }

                            if (!r.packet.isWaitForMultipleResponses()) {
                                i.remove();
                                continue;
                            }
                        }

                        if (r.sentTimestamp + RESPONSE_MAX_DELAY < System.currentTimeMillis()) {
                            if (!r.packet.isWaitForMultipleResponses()) {
//                                logger.warning("No response for " + r.packet.getId() + " (" + r.packet.getClass() + ")");
                                r.fireOnError(ErrorCode.TIMEOUT, "Нет ответа от контроллера: " + r.packet.getMacAddress());
                            }
                            else {
                                r.fireOnDone();
                            }

                            i.remove();
                            continue;
                        }
                    }
                }

                if (response != null && !packetDispathed) {
                    logger.warn("Packet {} dropped", response.getId());
                }
            }
        }

        synchronized void putRequest(Request r) {
            queue.add(r);
        }

        private final Queue<Request> queue = new LinkedList<>();
        private final DatagramPacket buf = new DatagramPacket(
                new byte[MAX_PACKET_SIZE], MAX_PACKET_SIZE);
    }

    private class Request {

        Request(DeviceId deviceId, Packet packet,
                Class<? extends Packet> responseClass,
                OnReceive receiveHandler, OnError errorHandler,
                OnDone doneHandler) {
            this.deviceId = deviceId;
            this.packet = packet;
            this.responseClass = responseClass;
            this.receiveHandler = receiveHandler;
            this.errorHandler = errorHandler;
            this.doneHandler = doneHandler;
        }

        void fireOnReceive(final Packet p) {
            if (receiveHandler != null) {
                scheduleHandler(() -> receiveHandler.handle(p));
            }
        }

        void fireOnError(ErrorCode e, String message) {
            if (errorHandler != null) {
                scheduleHandler(() -> errorHandler.handle(e, message));
            }
        }

        void fireOnDone() {
            if (doneHandler != null) {
                scheduleHandler(() -> doneHandler.handle());
            }
        }

        private void scheduleHandler(Runnable runnable) {
            if (useInternalWorkerThread) {
                try {
                    workerThreadRunnable.tasks.put(runnable);
                }
                catch (InterruptedException e) {
                    logger.error("Receiver thread interrupted");
                }
            }
            else {
                Platform.runLater(runnable);
            }
        }

        @Override
        public String toString() {
            return "Request{"
                    + "deviceId=" + deviceId
                    + ", packet=" + packet
                    + ", sentTimestamp=" + sentTimestamp
                    + ", responseClass=" + responseClass.getSimpleName() + '}';
        }

        DeviceId deviceId;
        Packet packet;
        long sentTimestamp;
        Class<? extends Packet> responseClass;
        OnReceive receiveHandler;
        OnError errorHandler;
        OnDone doneHandler;
    }

    private final DatagramSocket socket;
    private final InetAddress broadcastAddress;
    private final BinarySerializer serializer;

    private volatile boolean sendBroadcast;

    private final Sender sender;
    private final Receiver receiver;

    private volatile boolean useInternalWorkerThread;
    private volatile WorkerThreadRunnable workerThreadRunnable;

    private static class WorkerThreadRunnable implements Runnable {

        @Override
        public void run() {
            while (true) {
                try {
                    tasks.take().run();
                }
                catch (Throwable e) {
                    logger.warn("Exception in receive handler");
                }
            }
        }

        final BlockingQueue<Runnable> tasks = new ArrayBlockingQueue<>(100, true);
    }

    private static Client instance;
    private static final Logger logger = LoggerFactory.getLogger(Client.class);

    private static final int
            SERVER_SOCKET_PORT = 5001,
            MAX_PACKET_SIZE = 2048,
            SOCKET_TIMEOUT = 2500,
            RESPONSE_MAX_DELAY = 4_000;
}
