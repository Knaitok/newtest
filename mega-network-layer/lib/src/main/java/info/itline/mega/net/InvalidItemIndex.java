package info.itline.mega.net;

public class InvalidItemIndex {

    private InvalidItemIndex() {
    }
    
    public static final Byte BYTE_VALUE = (byte)0xFF;
    public static final Short SHORT_VALUE = (short)0xFFFF;
}
