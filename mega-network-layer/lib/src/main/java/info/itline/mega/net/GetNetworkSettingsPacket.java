package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class GetNetworkSettingsPacket extends Packet {

    GetNetworkSettingsPacket() {
        setPacketType(PacketType.GET_NETWORK_SETTINGS);
    }
}
