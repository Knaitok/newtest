package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
public class TransientNumberPacket extends PersistentNumberPacket {

    TransientNumberPacket(short[] idx, int[] data) {
        super(idx, data);
        setPacketType(PacketType.TRANSIENT_NUMBER);
    }

    private TransientNumberPacket() {
        super(new short[0], new int[0]);
    }
    
    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.TRANSIENT_NUMBER);
    }
}
