package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

/**
 * Class describes the request for EEConstants from the device
 */
@BinarySerializable
public class GetEEConstantsPacket extends Packet {
    GetEEConstantsPacket() {
        setPacketType(PacketType.GET_EE_CONSTANTS);
    }
}
