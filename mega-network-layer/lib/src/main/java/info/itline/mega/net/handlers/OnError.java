package info.itline.mega.net.handlers;

public interface OnError {
    void onError(String message);
}
