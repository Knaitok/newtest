package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import java.util.GregorianCalendar;

@BinarySerializable
class PutDateTimeSettings extends DateTimeSettingsPacket {

    PutDateTimeSettings(GregorianCalendar calendar) {
        super(convertCalendarToStamp(calendar));
        setPacketType(PacketType.PUT_DATETIME_SETTINGS);
    }

    PutDateTimeSettings() {
        this(new GregorianCalendar());
    }
}
