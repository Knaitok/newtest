package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
public class TransientStringPacket extends Packet {

    TransientStringPacket(byte[] idx, byte[] profile, String[] strings) {
        assert(idx.length == profile.length);
        assert(profile.length == strings.length);
        assert(strings.length <= INDEX_COUNT);
        
        int len = idx.length;
        int nEmpty = INDEX_COUNT - len;
        
        this.idx = new byte[INDEX_COUNT];
        System.arraycopy(idx, 0, this.idx, 0, len);
        for (int i = 0; i < nEmpty; i++) {
            this.idx[len + i] = InvalidItemIndex.BYTE_VALUE;
        }
        
        this.profile = new byte[INDEX_COUNT];
        System.arraycopy(profile, 0, this.profile, 0, len);
        
        this.strings = new String[INDEX_COUNT];
        System.arraycopy(strings, 0, this.strings, 0, len);
        setPacketType(PacketType.TRANSIENT_STRING);
    }

    private TransientStringPacket() {
        this.idx = null;
        this.profile = null;
        this.strings = null;
    }
    
    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.TRANSIENT_STRING);
    }

    public byte[] getIdx() {
        return idx;
    }

    public byte[] getProfile() {
        return profile;
    }

    public String[] getStrings() {
        return strings;
    }
    
    @Serialize(offset = 0, itemCount = INDEX_COUNT)
    private final byte[] idx;
    @Serialize(offset = 1, itemCount = INDEX_COUNT)
    private final byte[] profile;
    @Serialize(offset = 2, maxLength = 128, itemCount = INDEX_COUNT)
    private final String[] strings;
            
    public static final int INDEX_COUNT = 8;
}
