package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

import java.util.List;

/**
 * A class that is used to identify the need to set images
 */
@BinarySerializable
public class PutImagePacket extends ImagePacket {

    public PutImagePacket(byte[] imageIndexes, List<byte[]> images) {
        super(imageIndexes, images);
        setPacketType(PacketType.PUT_IMAGE);
    }
}
