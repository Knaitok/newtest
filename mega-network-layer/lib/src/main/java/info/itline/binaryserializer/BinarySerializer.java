package info.itline.binaryserializer;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.NotSerializableException;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.*;

public class BinarySerializer {

    public static void main(String[] args) {
        
    }

    public BinarySerializer() {
        this(DEFAULT_CHARSET, DEFAULT_BYTE_ORDER, DEFAULT_INSERT_PREFIX);
    }
    
    public BinarySerializer(Charset c) {
        this(c, DEFAULT_BYTE_ORDER, DEFAULT_INSERT_PREFIX);
    }
    
    public BinarySerializer(Charset c, ByteOrder bo) {
        this(c, bo, DEFAULT_INSERT_PREFIX);
    }
    
    public BinarySerializer(Charset c, ByteOrder bo, boolean pref) {
        this.stringEncoding = c;
        this.byteOrder = bo;
        this.insertStringLengthPrefix = pref;
    }
    
    public BinarySerializer(String c) {
        this(c, DEFAULT_BYTE_ORDER, DEFAULT_INSERT_PREFIX);
    }
    
    public BinarySerializer(String c, ByteOrder bo) {
        this(c, bo, DEFAULT_INSERT_PREFIX);
    }
    
    public BinarySerializer(String c, ByteOrder bo, boolean pref) {
        this(Charset.forName(c), bo, pref);
    }
    
    public <T extends Object> void serialize(T[] o, OutputStream s, int version) 
            throws IOException {
        writeObjectsToStream(wrapOutputStream(s), version, o);
    }
    
    public <T extends Object> void serialize(T[] o, OutputStream s) 
            throws IOException {
        serialize(o, s, DEFAULT_VERSION);
    }
    
    public <T extends Object> byte[] serialize(T[] o, int version) 
            throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        serialize(o, baos, version);
        return baos.toByteArray();
    }
    
    public <T extends Object> byte[] serialize(T[] o) throws IOException {
        return serialize(o, DEFAULT_VERSION);
    }
    
    public <T extends Object> void serialize(T o, OutputStream s, int version) 
            throws IOException {
        T[] tmp = (T[])Array.newInstance(o.getClass(), 1);
        tmp[0] = o;
        serialize(tmp, s, version);
    }
    
    public <T extends Object> byte[] serialize(T o, int version) 
            throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        serialize(o, baos, version);
        return baos.toByteArray();
    }
    
    public <T extends Object> void serialize(T o, OutputStream s) 
            throws IOException {
        serialize(o, s, DEFAULT_VERSION);
    }
    
    public <T extends Object> byte[] serialize(T o) throws IOException {
        return serialize(o, DEFAULT_VERSION);
    }
    
    public <T extends Object> T[] deserialize(Class<T> c, int count, InputStream s, int version)
            throws IOException {
        T[] result = (T[])Array.newInstance(c, count);
        readObjectsFromStream(wrapInputStream(s), version, result);
        return result;
    }
    
    public <T extends Object> T[] deserialize(Class<T> c, int count, InputStream s)
            throws IOException {
        return deserialize(c, count, s, DEFAULT_VERSION);
    }
    
    public <T extends Object> T[] deserialize(Class<T> c, int count, byte[] data)
            throws IOException {
        return deserialize(c, count, new ByteArrayInputStream(data));
    }
    
    public <T extends Object> T deserialize(Class<T> c, InputStream s, int version)
            throws IOException {
        return deserialize(c, 1, s, version)[0];
    }
    
    public <T extends Object> T deserialize(Class<T> c, byte[] bytes, int version)
            throws IOException {
        return deserialize(c, new ByteArrayInputStream(bytes), version);
    }
    
    public <T extends Object> T deserialize(Class<T> c, InputStream s) 
            throws IOException {
        return deserialize(c, s, DEFAULT_VERSION);
    }
    
    public <T extends Object> T deserialize(Class<T> c, byte[] bytes) 
            throws IOException {
        return deserialize(c, bytes, DEFAULT_VERSION);
    }
    
    private <T extends Object> void writeObjectsToStream(DataOutput s, int version, T[] objs) 
            throws IOException {
        List<SerializableField> fields = getSerializableFieldListForHierarchy(
                getComponentType(objs), version);
        for (T o: objs) {
            for (SerializableField f: fields) {
                f.write(s, o);
            }
        }
        if (s instanceof Flushable) {
            ((Flushable)s).flush();
        }
    }
    
    private <T extends Object> void readObjectsFromStream(DataInput in, int version, T[] result) 
            throws IOException {
        Class<?> clazz = getComponentType(result);
        List<SerializableField> fields = getSerializableFieldListForHierarchy(clazz, version);
        Method postCreateMethod = null;
        try {
            postCreateMethod = clazz.getDeclaredMethod(POST_CREATE_METHOD_NAME);
            postCreateMethod.setAccessible(true);
        }
        catch (NoSuchMethodException ignore) {
            
        }
        for (int i = 0; i < result.length; i++) {
            T o = (T)instantiate(clazz);
            for (SerializableField f: fields) {
                f.read(in, o);
            }
            if (postCreateMethod != null) {
                try {
                    postCreateMethod.invoke(o);
                }
                catch (IllegalArgumentException | IllegalAccessException ignore) {
                    
                }
                catch (InvocationTargetException e) {
                    throw new PostCreateException(e.getCause());
                }
            }
            result[i] = o;
        }
    }
    
    private <T> Class<?> getComponentType(T[] ary) {
        return ary.getClass().getComponentType();
    }
    
    private DataOutput wrapOutputStream(OutputStream s) {
        return byteOrder == ByteOrder.LITTLE_ENDIAN ? 
                new LittleEndianDataOutputStream(s) :
                new DataOutputStream(s);
    }
    
    private DataInput wrapInputStream(InputStream s) {
        return byteOrder == ByteOrder.LITTLE_ENDIAN ?
                new LittleEndianDataInputStream(s) :
                new DataInputStream(s);
    }
    
    private static abstract class PropertyAccessor {

        PropertyAccessor(Class<?> clazz) {
            Class<?> ct;
            int dc = 0;
            for (ct = clazz; ct.isArray(); ct = ct.getComponentType()) {
                dc += 1;
            }
            type = ct;
            dimension = dc;
        }
        
        abstract Object get(Object o);
        abstract void set(Object o, Object v);
        abstract String getName();
        
        abstract Serialize getAnnotation();
        
        final Class<?> getType() {
            return type;
        }
        
        final int getDimension() {
            return dimension;
        }
        
        private final Class<?> type;
        private final int dimension;
    }
    
    private static class FieldAccessor extends PropertyAccessor {

        public FieldAccessor(Field f) {
            super(f.getType());
            this.field = f;
            f.setAccessible(true);
        }
        
        @Override
        Object get(Object o) {
            try {
                return field.get(o);
            }
            catch (IllegalAccessException ignore) {
                return null;
            } 
        }

        @Override
        void set(Object o, Object v) {
            try {
                field.set(o, v);
            }
            catch (IllegalAccessException ignore) {
                
            }
        }

        @Override
        String getName() {
            return field.getName();
        }

        @Override
        Serialize getAnnotation() {
            return getSerializeAnnotaion(field);
        }
        
        private final Field field;
    }
    
    private abstract class SerializableField {

        SerializableField(PropertyAccessor accessor, int[] itemCount) {
            assert(accessor.getDimension() == itemCount.length);
            this.accessor = accessor;
            this.itemCount = itemCount;
        }
        
        public final void read(DataInput in, Object o) throws IOException {
            Object value;
            if (itemCount.length == 0) {
                value = doRead(in);
            }
            else {
                value = readArray(in, 0);
            }
            setValue(o, value);
        }
        
        private Object readArray(DataInput in, int offset) throws IOException {
            Object result;
            int len = itemCount[offset];
            if (offset == itemCount.length - 1) {
                result = Array.newInstance(accessor.getType(), len);
                for (int i = 0; i < itemCount[offset]; i++) {
                    Array.set(result, i, doRead(in));
                }
            }
            else {
                int dimCount = itemCount.length - offset;
                int[] dims = new int[dimCount];
                System.arraycopy(itemCount, offset, dims, 0, dimCount);
                
                result = Array.newInstance(accessor.getType(), dims);
                for (int i = 0; i < len; i++) {
                    Array.set(result, i, readArray(in, offset + 1));
                }
            }
            return result;
        }
        
        public final void write(DataOutput out, Object o) throws IOException {
            Object v;
            if (o != null && (v = getValue(o)) != null) {
                if (itemCount.length == 0) {
                    doWrite(out, v);
                }
                else {
                    writeArray(v, 0, out);
                }
            }
            else {
                fillWithZeros(out, getSize());
            }
        }
        
        private void writeArray(Object ary, int offset, DataOutput out) throws IOException {
            if (ary == null) {
                int fillerLength = 1;
                for (int i = 0; i < itemCount.length - offset; i++) {
                    fillerLength *= itemCount[offset + i];
                }
                fillWithZeros(out, fillerLength * getComponentSize());
                return;
            }
            
            int len = itemCount[offset];
            if (Array.getLength(ary) != len) {
                throw new NotSerializableException(
                        "Array length does not match item count for" +
                        formatMemberName(accessor.getType(), accessor));
            }
            if (offset == itemCount.length - 1) {
                for (int i = 0; i < len; i++) {
                    Object elem = Array.get(ary, i);
                    if (elem != null) {
                        doWrite(out, Array.get(ary, i));
                    }
                    else {
                        fillWithZeros(out, getComponentSize());
                    }
                }
            }
            else {
                for (int i = 0; i < len; i++) {
                    writeArray(Array.get(ary, i), offset + 1, out);
                }
            }
        }
        
        private Object getValue(Object o) throws IOException {
            return accessor.get(o);
        }
        
        private void setValue(Object o, Object v) throws IOException {
            accessor.set(o, v);
        }
        
        public final int getSize() {
            int s = getComponentSize();
            for (int i = 0; i < itemCount.length; i++) {
                s *= itemCount[i];
            }
            return s;
        }
        
        protected void fillWithZeros(DataOutput o, int sz) 
                throws IOException {
            for (int i = 0; i < sz; i++) {
                o.writeByte((byte)0);
            }
        }
        
        protected abstract int getComponentSize();
        protected abstract Object doRead(DataInput in) throws IOException;
        protected abstract void doWrite(DataOutput out, Object o) throws IOException;
        
        final PropertyAccessor accessor;
        final int[] itemCount;
    }
    
    private class PrimitiveField extends SerializableField {
        
        PrimitiveField(PropertyAccessor accessor, int[] itemCount) {
            super(accessor, itemCount);
            checkType();
        }

        private void checkType() {
            Class<?> clazz = accessor.getType();
            assert((clazz.isPrimitive() || isBoxedPrimitive(clazz)) && 
                    !clazz.equals(char.class) && 
                    !clazz.equals(Character.class));
        }
        
        @Override
        protected Object doRead(DataInput in) throws IOException {
            Class<?> clazz = accessor.getType();
            if (clazz.equals(byte.class) || clazz.equals(Byte.class)) {
                return in.readByte();
            }
            else if (clazz.equals(short.class) || clazz.equals(Short.class)) {
                return in.readShort();
            }
            else if (clazz.equals(int.class) || clazz.equals(Integer.class)) {
                return in.readInt();
            }
            else if (clazz.equals(long.class) || clazz.equals(Long.class)) {
                return in.readLong();
            }
            else if (clazz.equals(float.class) || clazz.equals(Float.class)) {
                return in.readFloat();
            }
            else if (clazz.equals(double.class) || clazz.equals(Double.class)) {
                return in.readDouble();
            }
            else if (clazz.equals(boolean.class) || clazz.equals(Boolean.class)) {
                return in.readBoolean();
            }
            else {
                throw new RuntimeException("Not a primitive serializable type: " + clazz);
            }
        }

        @Override
        protected void doWrite(DataOutput out, Object o) throws IOException {
            Class<?> clazz = accessor.getType();
            if (clazz.equals(byte.class) || clazz.equals(Byte.class)) {
                out.writeByte((byte)o);
            }
            else if (clazz.equals(short.class) || clazz.equals(Short.class)) {
                out.writeShort((short)o);
            }
            else if (clazz.equals(int.class) || clazz.equals(Integer.class)) {
                out.writeInt((int)o);
            }
            else if (clazz.equals(long.class) || clazz.equals(Long.class)) {
                out.writeLong((long)o);
            }
            else if (clazz.equals(float.class) || clazz.equals(Float.class)) {
                out.writeFloat((float)o);
            }
            else if (clazz.equals(double.class) || clazz.equals(Double.class)) {
                out.writeDouble((double)o);
            }
            else if (clazz.equals(boolean.class) || clazz.equals(Boolean.class)) {
                out.writeBoolean((boolean)o);
            }
            else {
                throw new RuntimeException("Not a primary type: " + clazz);
            }
        }
        
        @Override
        protected int getComponentSize() {
            Class<?> clazz = accessor.getType();
            if (clazz.equals(byte.class) || clazz.equals(Byte.class)) {
                return Byte.SIZE / 8;
            }
            else if (clazz.equals(short.class) || clazz.equals(Short.class)) {
                return Short.SIZE / 8;
            }
            else if (clazz.equals(int.class) || clazz.equals(Integer.class)) {
                return Integer.SIZE / 8;
            }
            else if (clazz.equals(long.class) || clazz.equals(Long.class)) {
                return Long.SIZE / 8;
            }
            else if (clazz.equals(float.class) || clazz.equals(Float.class)) {
                return Float.SIZE / 8;
            }
            else if (clazz.equals(double.class) || clazz.equals(Double.class)) {
                return Double.SIZE / 8;
            }
            else if (clazz.equals(boolean.class) || clazz.equals(Boolean.class)) {
                return Byte.SIZE / 8;
            }
            else {
                throw new RuntimeException("Not a primary type: " + clazz);
            }
        }
    }
    
    private class StringField extends SerializableField {
        
        StringField(PropertyAccessor accessor, int[] itemCount, int maxLength) {
            super(accessor, itemCount);
            this.maxLength = maxLength;
            assert(accessor.getType().equals(String.class));
        }
        
        @Override
        protected Object doRead(DataInput in) throws IOException {
            byte[] bytes;
            int padding = maxLength;
            if (insertStringLengthPrefix) {
                int len = in.readInt();
                checkLength(len);
                bytes = new byte[len];
                in.readFully(bytes);
                padding -= len + Integer.BYTES;
            }
            else {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int b;
                while (true) {
                    b = in.readByte();
                    if (b != 0) {
                        baos.write(b);
                    }
                    else {
                        break;
                    }
                }
                bytes = cutByteString(baos.toByteArray());
                padding -= bytes.length + 1;
            }
            
            for (int i = 0; i < padding; i++) {
                in.readByte();
            }
            
            return stringEncoding.decode(ByteBuffer.wrap(bytes)).toString();
        }

        @Override
        protected void doWrite(DataOutput out, Object o) throws IOException {
            String str = (String)o;
            byte[] bytes = cutByteString(stringEncoding.encode(str).array());
            int padding = maxLength - str.length();
            if (insertStringLengthPrefix) {
                out.writeInt(str.length());
                padding -= Integer.BYTES;
            }
            out.write(bytes);
            if (!insertStringLengthPrefix) {
                out.writeByte(0);
                padding -= 1;
            }
            fillWithZeros(out, padding);
        }

        @Override
        protected int getComponentSize() {
            return maxLength;
        }
        
        private void checkLength(int len) throws NotSerializableException {
            if (len > maxLength) {throw new NotSerializableException("String is too long");
            }
        }

        private byte[] cutByteString(byte[] strBytes) {
            if(strBytes.length < maxLength)
                return strBytes;
            byte[] cutBytes = Arrays.copyOf(strBytes, maxLength);
            cutBytes[maxLength -1] = strBytes[strBytes.length - 1];
            return cutBytes;
        }
        
        final int maxLength;
    }
        
    private class ObjectField extends SerializableField {

        ObjectField(PropertyAccessor accessor, int[] itemCount, int version)
                throws NotSerializableException {
            super(accessor, itemCount);
            this.serializableFields = getSerializableFieldListForHierarchy(accessor.getType(), version);
            this.componentSize = calcaulateStructureSize(serializableFields);
        }
        
        private int calcaulateStructureSize(List<SerializableField> fields) {
            int result = 0;
            for (SerializableField f: fields) {
                result += f.getSize();
            }
            return result;
        }
        
        @Override
        protected Object doRead(DataInput in) throws IOException {
            Object result = instantiate(accessor.getType());
            for (SerializableField f: serializableFields) {
                f.read(in, result);
            }
            return result;
        }

        @Override
        protected void doWrite(DataOutput out, Object o) throws IOException {
            for (SerializableField f: serializableFields) {
                f.write(out, o);
            }
        }

        @Override
        protected int getComponentSize() {
            return componentSize;
        }
        
        final List<SerializableField> serializableFields;
        final int componentSize;
    }
    
    private class EnumField extends SerializableField {

        public EnumField(PropertyAccessor accessor, int[] itemCount) 
                throws NotSerializableException {
            super(accessor, itemCount);
            Class<?> clazz = accessor.getType();
            assert(clazz.isEnum());
            
            String className = "Enum " + clazz.getName();
            
            BinarySerializable a = clazz.getAnnotation(BinarySerializable.class);
            if (a == null) {
                throw new NotSerializableException(className + " isn't marked as @BinarySerializable");
            }
            
            int bc = a.byteCount();
            boolean validByteCount = bc == 1 || bc == 2 || bc == 4 || bc == 8;
            if (!validByteCount) {
                throw new NotSerializableException(className + " has invalid size");
            }
            
            this.byteCount = bc;
            this.valueType = getIntegerValueClass(byteCount);
            
            for (Method m: clazz.getDeclaredMethods()) {
                if (m.getAnnotation(ToInteger.class) != null &&
                        m.getParameterCount() == 0 &&
                        m.getReturnType().equals(valueType)) {
                    toIntegerMethod = m;
                }
                if (m.getAnnotation(FromInteger.class) != null && 
                        Modifier.isStatic(m.getModifiers()) && 
                        m.getParameterCount() == 1 &&
                        m.getParameterTypes()[0].equals(valueType) &&
                        m.getReturnType().equals(clazz)) {
                    fromIntegerMethod = m;
                }
            }
            
            if (toIntegerMethod == null) {
                throw new NotSerializableException(className + " should define @ToInteger method");
            }
            
            if (fromIntegerMethod == null) {
                throw new NotSerializableException(className + " should define @FromInteger method");
            }
            
            toIntegerMethod.setAccessible(true);
            fromIntegerMethod.setAccessible(true);
        }
        
        private Class<? extends Number> getIntegerValueClass(int byteCount) {
            if (byteCount == 1) {
                return byte.class;
            }
            else if (byteCount == 2) {
                return short.class;
            }
            else if (byteCount == 4) {
                return int.class;
            }
            else if (byteCount == 8) {
                return long.class;
            }
            else {
                throw new RuntimeException("Illegal byte count");
            }
        }

        @Override
        protected int getComponentSize() {
            return byteCount;
        }

        @Override
        protected Object doRead(DataInput in) throws IOException {
            Object v;
            if (byteCount == 1) {
                v = in.readByte();
            }
            else if (byteCount == 2) {
                v = in.readShort();
            }
            else if (byteCount == 4) {
                v = in.readInt();
            }
            else {
                v = in.readLong();
            }
            
            Object result = null;
            try {
                result = fromIntegerMethod.invoke(null, v);
                if (result == null) {
                    throw new IOException("Enum " + accessor.getType() + 
                            " has no member with value " + v);
                }
            }
            catch (ClassCastException | IllegalAccessException ignore) {
                
            }
            catch (InvocationTargetException e) {
                throw new IOException("Cannot convert integer value to enum member for " 
                        + accessor.getType() + ": " + e.getCause().getMessage());
            }
           
            return result;
        }

        @Override
        protected void doWrite(DataOutput out, Object o) throws IOException {
            Object v = null;
            try {
                v = toIntegerMethod.invoke(o);
            }
            catch (IllegalAccessException ignore) {
                
            }
            catch (InvocationTargetException e) {
                throw new IOException("Cannot convert enum member to integer for " 
                        + accessor.getType() + ": " + e.getCause().getMessage());
            }
            if (byteCount == 1) {
                out.writeByte(Byte.class.cast(v));
            }
            else if (byteCount == 2) {
                out.writeShort(Short.class.cast(v));
            }
            else if (byteCount == 4) {
                out.writeInt(Integer.class.cast(v));
            }
            else {
                out.writeLong(Long.class.cast(v));
            }
        }
        
        private final int byteCount;
        private Method toIntegerMethod;
        private Method fromIntegerMethod;
        private Class<? extends Number> valueType; 
    }
    
    private List<SerializableField> getSerializableFieldListForHierarchy(
            Class<?> clazz, int version) throws NotSerializableException {
        List<Class<?>> hierarchy = getClassHierarchyForClass(clazz);
        checkSerializable(hierarchy);

        List<SerializableField> result = new ArrayList<>();

        for (Class<?> c: hierarchy) {
            result.addAll(getSerializableFieldListForClass(c, version));
        }

        return result;
    }

    private List<SerializableField> getSerializableFieldListForClass(
            Class<?> clazz, int version) throws NotSerializableException {
        List<SerializableField> result = new ArrayList<>();

        for (PropertyAccessor a: getSortedMarkedFields(clazz, version)) {
            Serialize s = a.getAnnotation();
            Class<?> itemClass = a.getType();
            
            int[] itemCount;
            if (a.getDimension() == 0) {
                itemCount = new int[0];
            }
            else {
                itemCount = s.itemCount();
                
                if (itemCount.length != a.getDimension()) {
                    throw new NotSerializableException(
                            "Item count doesn't match array dimensions for " + 
                            formatMemberName(clazz, a));
                }
                
                for (int i: itemCount) {
                    if (i <= 0) {
                        throw new NotSerializableException(
                                "Item count is negative for " +
                                formatMemberName(clazz, a));
                    }
                }
            }
            
            if (itemClass.isPrimitive() || isBoxedPrimitive(itemClass)) {
                result.add(new PrimitiveField(a, itemCount));
            }
            else if (itemClass.equals(String.class)) {
                int maxLength = s.maxLength();
                if (maxLength < 0) {
                    throw new NotSerializableException(
                            "Max length is negative for " +
                            formatMemberName(clazz, a));
                }
                result.add(new StringField(a, itemCount, maxLength));
            }
            else if (itemClass.isEnum()) {
                result.add(new EnumField(a, itemCount));
            }
            else {
                result.add(new ObjectField(a, itemCount, version));
            }
        }

        return result;
    }

    private static List<PropertyAccessor> getSortedMarkedFields(Class<?> c, int version) 
            throws NotSerializableException {
        List<PropertyAccessor> result = new ArrayList<>();
        for (Field f: c.getDeclaredFields()) {
            Serialize a = f.getAnnotation(Serialize.class);
            
            if (getSerializeAnnotaion(f) == null) {
                continue;
            }
            
            int[] versions = a.versions();
            boolean serializeInThisVersion = false;
            for (int i = 0; i < versions.length; i++) {
                if (version == versions[i]) {
                    serializeInThisVersion = true;
                    break;
                }
            }
            
            if (serializeInThisVersion) {
                result.add(new FieldAccessor(f));
            }
        }

        Collections.sort(result, (o1, o2) -> {
            return  o1.getAnnotation().offset() - o2.getAnnotation().offset();
        });

        checkOffsets(result);

        return result;
    }

    private Object instantiate(Class<?> clazz) throws NotSerializableException {
        try {
            Constructor<?> c = clazz.getDeclaredConstructor();
            c.setAccessible(true);
            return c.newInstance();
            // return clazz.newInstance();
        }
        catch (Exception e) {
            NotSerializableException nse = new NotSerializableException(e.getClass().getName() + ": " + clazz.getName());
            nse.initCause(e);
            throw nse;
        }
    }

    private static String formatMemberName(Class<?> c, PropertyAccessor f) {
        return c.getName() + "." + f.getName();
    }

    private static boolean isBoxedPrimitive(Class<?> clazz) {
        return clazz.equals(Byte.class) || clazz.equals(Short.class) ||
                clazz.equals(Integer.class) || clazz.equals(Long.class) ||
                clazz.equals(Float.class) || clazz.equals(Double.class) ||
                clazz.equals(Boolean.class) || clazz.equals(Character.class);
    }

    private static List<Class<?>> getClassHierarchyForClass(Class<?> clazz) {
        List<Class<?>> result = new LinkedList<>();
        for (Class<?> c = clazz; !c.equals(Object.class); c = c.getSuperclass()) {
            result.add(0, c);
        }
        return result;
    }

    private static boolean isBinarySerializable(List<Class<?>> h) {
        for (Class<?> c: h) {
            if (c.getAnnotation(BinarySerializable.class) == null) {
                return false;
            }
        }
        return true;
    }

    private static void checkSerializable(List<Class<?>> hierarchy) 
            throws NotSerializableException {
        if (hierarchy.isEmpty() || !isBinarySerializable(hierarchy)) {
            StringBuilder b = new StringBuilder();
            for (Class<?> c: hierarchy) {
                b.append(" ").append(c.getName());
            }
            throw new NotSerializableException(
                    "Object class or one of it's superclasses or " +
                    "one of it's fields is not @BinarySerializable:" + b);
        }
    }

    private static void checkOffsets(List<PropertyAccessor> accessors) 
            throws NotSerializableException {
        for (int i = 0; i < accessors.size() - 1; i++) {
            PropertyAccessor a = accessors.get(i);
            PropertyAccessor nextA = accessors.get(i + 1);
            if (a.getAnnotation().offset() == nextA.getAnnotation().offset()) {
                throw new NotSerializableException(
                        "Two or more fields have same offsets: "  + 
                                a.getName() + " and " + nextA.getName());
            }
        }
    }

    private static Serialize getSerializeAnnotaion(Field f) {
        return f.getAnnotation(Serialize.class);
    }
    
    public ByteOrder getByteOrder() {
        return byteOrder;
    }

    public void setByteOrder(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }
    
    public Charset getStringEncoding() {
        return stringEncoding;
    }

    public void setStringEncoding(Charset stringEncoding) {
        this.stringEncoding = stringEncoding;
    }

    public boolean getInsertStringLengthPrefix() {
        return insertStringLengthPrefix;
    }

    public void setInsertStringLengthPrefix(boolean insertStringLengthPrefix) {
        this.insertStringLengthPrefix = insertStringLengthPrefix;
    }
    
    private ByteOrder byteOrder;
    private Charset stringEncoding;
    private boolean insertStringLengthPrefix;
    
    private static final ByteOrder DEFAULT_BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;
    private static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
    private static final boolean DEFAULT_INSERT_PREFIX = false;
    
    public static final int DEFAULT_VERSION = 0;
    
    public static final String POST_CREATE_METHOD_NAME = "postCreate";
}