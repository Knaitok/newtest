package info.itline.controller;

class GetStopServerConfigRequestPacket extends RequestPacket {
    
    GetStopServerConfigRequestPacket(DeviceSettings settings) {
        super(RequestPacketType.GET_DYNAMIC_DATA, settings.getType(),
                settings.getMACAddress());
    }
}