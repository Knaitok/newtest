package info.itline.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DeviceSettings {
    public DeviceSettings(DeviceSettings other) {
        updateWith(other);
    }

    private DeviceSettings() {
        
    }
    
    public static DeviceSettings createFakeDeviceSettings() {
        DeviceSettings ds = new DeviceSettings();
        ds.macAddress = new MACAddress(new byte[] {1, 2, 3, 4, 5, 6});
        ds.type = DeviceType.CURRENCY_RATE_BOARD;
        ds.name = new DeviceName("FakeDevice");
        ds.ip = IPAddress.parse("127.0.0.1");
        ds.mask = IPAddress.parse("255.255.255.0");
        ds.gateway = IPAddress.parse("127.0.0.1");
        ds.isDisplayOff = false;
        ds.brightness = new DisplayBrightness(5);
        ds.displayDurationData = new DisplayDurationData(new int[DisplayDurationData.SIZE]);
        ds.userData = new UserData(new int[UserData.SIZE]);
        ds.versionMinor = 30;
        ds.versionMajor = 4;
        ds.manufacturingDate = new GregorianCalendar();
        ds.dateTime = new GregorianCalendar();
        ds.deviceMode = DeviceMode.NORMAL;
        ds.serverConfig = ServerConfig.createFakeConfig();
        return ds;
    }
    
    DeviceSettings(SearchResponsePacket packet) {
        macAddress = new MACAddress(packet.getMACAddress());
        type = packet.getDeviceType();
        name = new DeviceName(packet.getName());
        ip = new IPAddress(packet.getIp());
        mask = new IPAddress(packet.getMask());
        gateway = new IPAddress(packet.getGateway());
        isDisplayOff = packet.getIsDisplayOff() != 0 ? true : false;
        brightness = packet.getBrightness().clone();
        if (packet.getDeviceType() == DeviceType.CURRENCY_RATE_BOARD) {
            displayDurationData = new DisplayDurationData(packet.getDisplayDurationData());
            userData = new UserData(packet.getUserData());
        }
        int version = packet.getVersion();
        versionMinor = version & 0xFF;
        versionMajor = (version >> 8) & 0xFF;
        manufacturingDate = getEpochDate();
        manufacturingDate.add(Calendar.DAY_OF_MONTH, packet.getManufacturingDate());
        dateTime = getEpochDate();
        deviceMode = packet.getDeviceMode();
    }
    
    public final void updateWith(DeviceSettings other) {
        this.macAddress = new MACAddress(other.macAddress);
        this.setType(other.getType());
        this.name = new DeviceName(other.name);
        this.ip = new IPAddress(other.ip);
        this.mask = new IPAddress(other.getMask());
        this.gateway = new IPAddress(other.gateway);
        if (type == DeviceType.CURRENCY_RATE_BOARD) {
            this.displayDurationData= new DisplayDurationData(other.displayDurationData);
            this.userData = new UserData(other.userData);
            if (UDPClient.CAN_UPDATE_FROM_SERVER) {
                this.serverConfig = new ServerConfig(other.serverConfig);
            }
        }
        if (UDPClient.IS_STOP_BOARD) {
            this.stopServerConfig = new StopServerConfig(other.stopServerConfig);
        }
        this.isDisplayOff = other.isDisplayOff;
        this.brightness = other.brightness.clone();
        this.versionMinor = other.versionMinor;
        this.versionMajor = other.versionMajor;
        this.manufacturingDate = other.manufacturingDate;
        this.dateTime = (GregorianCalendar) other.dateTime.clone();
        this.deviceMode = other.deviceMode;
        this.creepingLineNumber = other.creepingLineNumber;
        this.creepingLineParam = other.creepingLineParam;
        this.creepingLineStatus = other.creepingLineStatus;
        this.creepingLineText = new CreepingLineText(other.creepingLineText);
    }
    
    @Override
    public String toString() {
        return String.format("%s - %s (%s, %s)", macAddress, getIp().toString(), name, 
                formatVersion());
    }
    
    public String toStringFull() {
        return String.format("[MAC = %s, Type = %s, Name = %s, IP = %s, "
                + "Mask = %s, Gateway = %s, "
                + "Display OFF = %b, Brigthess = %d, Version = %s, "
                + "Manufaturing Date = %s, Date and Time = %s]", 
                getMACAddress(), getType(), getName(), getIp(), getMask(), 
                getGateway(),
                getIsDisplayOff(), getBrigtness().getValue(), formatVersion(), 
                formatManufacturingDate(), formatDateTime());
    }
    
    public String formatManufacturingDate() {
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
        return fmt.format(manufacturingDate.getTime());
    }
    
    public String formatDateTime() {
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        return fmt.format(dateTime.getTime());
    }
    
    public String formatVersion() {
        return String.format("%d.%d", versionMajor, versionMinor);
    }


    public DeviceMode getDeviceMode() {
        return deviceMode;
    }

    public void setDeviceMode(DeviceMode mode) {
        this.deviceMode = mode;
    }

    public long getCreepingLineNumber() {
        return creepingLineNumber;
    }

    public void setCreepingLineNumber(long number) {
        this.creepingLineNumber = number;
    }

    public long getCreepingLineParam() {
        return creepingLineParam;
    }

    public void setCreepingLineParam(long param) {
        this.creepingLineParam = param;
    }
    
    void setCreepingLineStatus(long status) {
        this.creepingLineStatus = status;
    }

    public long getCreepingLineStatus() {
        return creepingLineStatus;
    }

    public CreepingLineText getCreepingLineText() {
        return creepingLineText;
    }

    public void setCreepingLineText(CreepingLineText creepingLineText) {
        this.creepingLineText = creepingLineText;
    }
    
    public MACAddress getMACAddress() {
        return macAddress;
    }
    
    void setMacAddress(MACAddress address) {
        this.macAddress = new MACAddress(address);
    }

    public DeviceName getName() {
        return name;
    }
    
    public void setName(DeviceName name) {
        this.name = name;
    }

    public IPAddress getIp() {
        return ip;
    }

    public void setIp(IPAddress ip) {
        this.ip = ip;
    }

    public IPAddress getMask() {
        return mask;
    }

    public void setMask(IPAddress mask) {
        this.mask = mask;
    }

    public IPAddress getGateway() {
        return gateway;
    }

    public void setGateway(IPAddress gateway) {
        this.gateway = gateway;
    }

    public DisplayDurationData getDisplayDurationData() {
        return displayDurationData;
    }
    
    public void setDisplayDurationData(DisplayDurationData data) {
        this.displayDurationData = new DisplayDurationData(data);
    }
    
    public boolean getIsDisplayOff() {
        return isDisplayOff;
    }

    public void setIsDisplayOff(boolean isDisplayOff) {
        this.isDisplayOff = isDisplayOff;
    }

    public DisplayBrightness getBrigtness() {
        return brightness;
    }

    public void setBrigtness(DisplayBrightness b) {
        this.brightness = b.clone();
    }

    public UserData getUserData() {
        return userData;
    }
    
    public void setUserData(UserData userData) {
        this.userData = new UserData(userData);
    }
    
    public Calendar getDateTime() {
        return dateTime;
    }
    
    public long getDateTimeAsSeconds() {
        return (dateTime.getTimeInMillis() - getEpochDate().getTimeInMillis()) / 1000;
    }
    
    public void setDateTime(long secSinceEpoch) {
        Calendar c = getEpochDate();
        c.add(Calendar.SECOND, (int) (secSinceEpoch & 0x7FFFFFFF));
        this.dateTime = c;
    }

    public void setDateTime(Calendar dateTime) {
        this.dateTime = dateTime;
    }
    
    private static Calendar getEpochDate() {
        return new GregorianCalendar(EPOCH_YEAR, EPOCH_MONTH, EPOCH_DAY_OF_MONTH);
    }
    
    public DeviceType getType() {
        return type;
    }

    public void setType(DeviceType type) {
        this.type = type;
    }

    public ServerConfig getServerConfig() {
        return serverConfig;
    }

    public void setServerConfig(ServerConfig serverConfig) {
        this.serverConfig = serverConfig;
    }

    public StopServerConfig getStopServerConfig() {
        return stopServerConfig;
    }

    public void setStopServerConfig(StopServerConfig stopServerConfig) {
        this.stopServerConfig = stopServerConfig;
    }
    
    /* public void setCreepingLineTextInt(long value) {
        byte[] buf = new byte[4]; 
        for (int i = 0; i < 4; i++) {
            buf[i] = (byte) ((value >> (i * 8)) & 0xFF);
        }
        try {
            String s = new String(buf, "ASCII");
            creepingLineText = new CreepingLineText(s);
        }
        catch (UnsupportedEncodingException e) {
            // ignore
        }
    } */
    
    private MACAddress macAddress;
    private DeviceType type;
    private DeviceName name;
    private IPAddress ip;
    private IPAddress mask;
    private IPAddress gateway;
    private DisplayDurationData displayDurationData;
    private boolean isDisplayOff;
    private DisplayBrightness brightness;
    private UserData userData;
    private int versionMinor;
    private int versionMajor;
    private Calendar manufacturingDate;
    private Calendar dateTime;
    private DeviceMode deviceMode;
    private long creepingLineNumber;
    private long creepingLineParam;
    private long creepingLineStatus;
    private CreepingLineText creepingLineText = new CreepingLineText("");
    
    private ServerConfig serverConfig;
    private StopServerConfig stopServerConfig;
    
    private static final int 
            EPOCH_YEAR = 2000,
            EPOCH_MONTH = 0,
            EPOCH_DAY_OF_MONTH = 1;
}
