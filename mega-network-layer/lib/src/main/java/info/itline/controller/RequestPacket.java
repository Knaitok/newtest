package info.itline.controller;

import info.itline.controller.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

abstract class RequestPacket {
    private MACAddress macAddress;
    private RequestPacketType requestPacketType;
    private DeviceType deviceType;
    
    private static final Logger logger = LoggerFactory.getLogger(RequestPacket.class);

    public RequestPacket(RequestPacketType requestPacketType,
            DeviceType deviceType, MACAddress macAddress) {
        this.requestPacketType = requestPacketType;
        this.deviceType = deviceType;
        this.macAddress = macAddress;
    }
    
    MACAddress getMACAddress() {
        return macAddress;
    }
    
    RequestPacketType getRequestPacketType() {
        return requestPacketType;
    }
    
    DeviceType getDeviceType() {
        return deviceType;
    }
    
    final byte[] getData() {
        try {
            return makeOutputStream().toByteArray();
        }
        catch (IOException exc) {
            // Exception indicates a bug in internal logic, we cannot do
            // anything meaningful except die
            logger.error("IOException raised while creating data stream");
            Util.die();
            return null;
        }
    }
    
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream res = new PacketBodyStream();
        macAddress.writeToStream(res);
        requestPacketType.writeToStream(res);
        deviceType.writeToStream(res);
        return res;
    }
}
