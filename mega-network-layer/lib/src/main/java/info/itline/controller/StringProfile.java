package info.itline.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public enum StringProfile implements Serializable {
    NONE(0, "Не определен", 0),
    RTC_TIME(1, "Время", 1),
    RTC_DATE(2, "Дата", 2),
    RTC_HH_MM(3, "ЧЧ:ММ", 3),
    RTC_HH_MM_SS(4, "ЧЧ:ММ:СС", 4),
    RTC_MM_SS(5, "ММ:СС", 5),
    RTC_HH(6, "ЧЧ", 6),
    RTC_MM(7, "ММ", 7),
    RTC_SS(8, "СС", 8),
    RTC_DD_MN(9, "ДД:МС", 9),
    RTC_DD_MN_YY(10, "ДД:МС:ГГ", 10),
    RTC_DD_MN_YYYY(11, "ДД:МС:ГГГГ", 11),
    RTC_YY(12, "ГГ", 12),
    RTC_YYYY(13, "ГГГГ", 13),
    RTC_MN(14, "МС", 14),
    RTC_DD(15, "ДД", 15),
    RTC_DOW(16, "ДН", 16),
    RTC_DOW_MN_DD(17, "ДН:МС:ДД", 17),
    RTC_DOW_MN_DD_YY(18, "ДН:ММ:ДД:ГГ", 18),
    RTC_DOW_MN_DD_YYYY(19, "ДН:ММ:ДД:ГГГГ", 19),
    STR_LEFT(20, "Строка по лев. краю", 1),
    STR_CENTER(21, "Строка по центру", 2),
    STR_RIGHT(22, "Строка по прав. краю", 3),
    STR_LSCROLL(23, "Строка по лев. краю с прокр.", 4),
    STR_CSCROLL(24, "Строка по центру с прокр.", 5),
    STR_RSCROLL(25, "Строка по прав. краю с прокр.", 6),
    STR_LOW_SPEED(26, "Бегущая строка с низ. скор.", 7),
    STR_MEDIUM_SPEED(27, "Бегущая строка со сред. скор.", 8),
    STR_HIGH_SPEED(28, "Бегущая строка с выс. скор.", 9),
    STR_CUSTOM_SPEED(29, "Бегущая строка с заданной скор.", 10),
    I_8_L(30, ".8 по лев. краю", 65),
    I_88_L(31, ".88 по лев. краю", 66),
    I_888_L(32, ".888 по лев. краю", 67),
    I_8888_L(33, ".8888 по лев. краю", 68),
    I_8_C(34, ".8 по центру", 81),
    I_88_C(35, ".88 по центру", 82),
    I_888_C(36, ".888 по центру", 83),
    I_8888_C(37, ".8888 по центру", 84),
    I_8_R(38, ".8 по прав. краю", 81),
    I_88_R(39, ".88 по прав. краю", 82),
    I_888_R(40, ".888 по прав. краю", 83),
    I_8888_R(41, ".8888 по прав. краю", 84),
    INT_LEFT(42, "Число по лев. краю", 1),
    INT_CENTER(43, "Число по центру", 2),
    INT_RIGHT(44, "Число по прав. краю", 3),
    INT_RIGHT_ZERO(45, "Число по прав. краю с вед. нулями", 4),
    AUTO(46, "Автоматически", 1),
    ILLUMINATION_ADC(47, "Освещенность, абс. значение", 1),
    ILLUMINATION_REL(48, "Освещенность, день/сумерки/ночь", 2),
    TIMER_SS(49, "Таймер, СС", 1),
    TIMER_SS_MS(50, "Таймер, СС:МС", 2),
    TIMER_MM_SS(51, "Таймер, ММ:СС", 3),
    TIMER_MM_SS_MS(52, "Таймер, ММ:СС:МС", 4),
    TIMER_HH_MM_SS(53, "Таймер, ЧЧ:ММ:СС", 5),
    TIMER_HH_MM_SS_MS(54, "Таймер, ЧЧ:ММ:СС:МС", 6),
    INT_DAY_COUNTER(55, "Счетчик дней", 5),
    INT_DAY_COUNTER_FWD(56, "Счетчик дней, возр.", 6),
    INT_DAY_COUNTER_BKW(57, "Счетчик дней, убыв.", 7);

    private String mRepr;
    private int mValue;
    private int mId;
    public static final int SIZE = 1;
    private static final Map<DataSource, StringProfile[]> StringProfileMappings = new HashMap();
    private static final Map<Integer, StringProfile> map = new HashMap();

    private StringProfile(int id, String repr, int value) {
        this.mId = id;
        this.mRepr = repr;
        this.mValue = value;
    }

    public int getValue() {
        return this.mValue;
    }

    public String toString() {
        return this.mRepr;
    }

    public String getName() {
        return super.toString();
    }

    public int getId() {
        return this.mId;
    }

    public static StringProfile[] getAvailableStringProfilesForDataSource(DataSource d) {
        StringProfile[] result = (StringProfile[])StringProfileMappings.get(d);
        if(result == null) {
            throw new RuntimeException("StringProfile list is not specified for " + d);
        } else {
            return result;
        }
    }

    public static StringProfile getById(int id) throws ClassNotFoundException {
        StringProfile result = (StringProfile)map.get(Integer.valueOf(id));
        if(result == null) {
            throw new ClassNotFoundException();
        } else {
            return result;
        }
    }

    static {
        StringProfileMappings.put(DataSource.NONE, new StringProfile[]{NONE});
        StringProfile[] rtcStringProfiles = new StringProfile[]{RTC_TIME, RTC_DATE, RTC_HH_MM, RTC_HH_MM_SS, RTC_MM_SS, RTC_HH, RTC_MM, RTC_SS, RTC_DD_MN, RTC_DD_MN_YY, RTC_DD_MN_YYYY, RTC_YY, RTC_YYYY, RTC_MN, RTC_DD, RTC_DOW, RTC_DOW_MN_DD, RTC_DOW_MN_DD_YY, RTC_DOW_MN_DD_YYYY};
        StringProfileMappings.put(DataSource.RTC, rtcStringProfiles);
        StringProfile[] timerStringProfiles = new StringProfile[]{TIMER_SS, TIMER_SS_MS, TIMER_MM_SS, TIMER_MM_SS_MS, TIMER_HH_MM_SS, TIMER_HH_MM_SS_MS};
        StringProfileMappings.put(DataSource.TIMER, timerStringProfiles);
        StringProfile[] stringStringProfiles = new StringProfile[]{STR_LEFT, STR_CENTER, STR_RIGHT, STR_LSCROLL, STR_CSCROLL, STR_RSCROLL, STR_LOW_SPEED, STR_MEDIUM_SPEED, STR_HIGH_SPEED, STR_CUSTOM_SPEED};
        StringProfile[] integerStringProfiles = new StringProfile[]{I_8_L, I_88_L, I_888_L, I_8888_L, I_8_C, I_88_C, I_888_C, I_8888_C, I_8_R, I_88_R, I_888_R, I_8888_R, INT_LEFT, INT_CENTER, INT_RIGHT, INT_RIGHT_ZERO, INT_DAY_COUNTER, INT_DAY_COUNTER_FWD, INT_DAY_COUNTER_BKW};
        StringProfileMappings.put(DataSource.PERSISTENT_STRING, stringStringProfiles);
        StringProfileMappings.put(DataSource.TRANSIENT_STRING, stringStringProfiles);
        StringProfileMappings.put(DataSource.CONST_STRING, stringStringProfiles);
        StringProfileMappings.put(DataSource.PERSISTENT_INT, integerStringProfiles);
        StringProfileMappings.put(DataSource.TRANSIENT_INT, integerStringProfiles);
        StringProfileMappings.put(DataSource.CONST_INT, integerStringProfiles);
        StringProfileMappings.put(DataSource.STOP_LINE, stringStringProfiles);
        StringProfileMappings.put(DataSource.STOP_ROUTE, stringStringProfiles);
        StringProfileMappings.put(DataSource.STOP_STOP_NAME, stringStringProfiles);
        StringProfileMappings.put(DataSource.STOP_TIME_REMAINIG_TIME, stringStringProfiles);
        StringProfile[] autoStringProfile = new StringProfile[]{AUTO};
        StringProfileMappings.put(DataSource.STOP_LOGO, autoStringProfile);
        StringProfileMappings.put(DataSource.TEMPERATURE, autoStringProfile);
        StringProfileMappings.put(DataSource.PRESSURE, autoStringProfile);
        StringProfileMappings.put(DataSource.HUMIDITY, autoStringProfile);
        StringProfileMappings.put(DataSource.RADIATION, autoStringProfile);
        StringProfileMappings.put(DataSource.ILLUMINATION, new StringProfile[]{ILLUMINATION_ADC, ILLUMINATION_REL});
        StringProfile[] imgStringProfile = new StringProfile[]{NONE};
        StringProfileMappings.put(DataSource.CONST_IMAGE, imgStringProfile);
        StringProfileMappings.put(DataSource.TRANSIENT_IMAGE, imgStringProfile);
        StringProfileMappings.put(DataSource.PERSISTENT_IMAGE, imgStringProfile);
        StringProfile[] var6 = values();
        int var7 = var6.length;

        for(int var8 = 0; var8 < var7; ++var8) {
            StringProfile f = var6[var8];
            map.put(Integer.valueOf(f.getId()), f);
        }
    }
}
