package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

public class PageOnServerString extends StringData {

    public PageOnServerString(String s) {
        super(s);
    }

    public PageOnServerString(LittleEndianDataInputStream in) throws IOException {
        super(in);
    }

    public PageOnServerString(StringData other) {
        super(other);
    }

    @Override
    protected int getMaxSize() {
        return 256;
    }
}
