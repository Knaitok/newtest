package info.itline.controller;

import java.io.IOException;
import java.io.OutputStream;

enum RequestPacketType {
    SEARCH                          (1),
    GET_INFO                        (3),
    GET_NETWORK_SETTINGS            (4),
    PUT_NETWORK_SETTINGS            (6),
    GET_NVRAM_DATA                  (7),
    PUT_NVRAM_DATA                  (9),
    GET_DYNAMIC_DATA                (10),
    PUT_DYNAMIC_DATA                (12),
    GET_DATE_TIME_SETTINGS          (13),
    PUT_DATE_TIME_SETTINGS          (15),
    PUT_CREEPING_LINE_SETTINGS      (16),
    START_DEBUG_LOGGING             (18),
    GET_SERVER_CONFIG               (20),
    PUT_SERVER_CONFIG               (22),
    FLASHING_COMMAND                (100),
    FLASHING_DATA                   (102);
    
    RequestPacketType(int id) {
        this.id = id;
    }
    
    int getId() {
        return id;
    }
    
    void writeToStream(OutputStream s) throws IOException {
        s.write((byte) id);
    }
    
    private int id;
}