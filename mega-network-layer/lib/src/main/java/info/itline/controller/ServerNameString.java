package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

public class ServerNameString extends StringData {

    public ServerNameString(String s) {
        super(s);
    }

    public ServerNameString(LittleEndianDataInputStream in) throws IOException {
        super(in);
    }

    public ServerNameString(StringData other) {
        super(other);
    }

    @Override
    protected int getMaxSize() {
        return 128;
    }
}
