package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ServerConfigResponsePacket extends ResponsePacket {

    public ServerConfigResponsePacket(LittleEndianDataInputStream data) throws Exceptions.InvalidPacket {
        super(data);
        checkPacketType(ResponsePacketType.SERVER_CONFIG, logger);
        try {
            serverConfig = new ServerConfig();
            serverConfig.setUpdatingPeriod(data.readInt());
            serverConfig.setWatchdogPeriod(data.readInt());
            serverConfig.setServerPort(data.readInt());
            serverConfig.setControllerId(data.readInt());
            serverConfig.setServerName(new ServerNameString(data));
            serverConfig.setPageOnServer(new PageOnServerString(data));
            serverConfig.setHostName(new HostNameString(data));
        }
        catch (IOException e) {
            handleIOException(e, logger);
        }
    }
    
    public ServerConfig getServerConfig() {
        return serverConfig;
    }
    
    private ServerConfig serverConfig;
    
    private static final Logger logger = LoggerFactory.getLogger(
            ServerConfigResponsePacket.class);
}
