package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DynamicDataResponsePacket extends ResponsePacket {
    DynamicDataResponsePacket(LittleEndianDataInputStream body) throws Exceptions.InvalidPacket {
        super(body);
        checkPacketType(ResponsePacketType.DYNAMIC_DATA, logger);
        // checkDeviceType(log);
        try {
            displayDurationData = new DisplayDurationData(body);
        }
        catch (IOException exc) {
            handleIOException(exc, logger);
        }
    }
    
    public DisplayDurationData getDisplayDurationData() {
        return displayDurationData;
    }
        
    private DisplayDurationData displayDurationData;
        
    private static final Logger logger = LoggerFactory.getLogger(
            DynamicDataResponsePacket.class);
}
