package info.itline.controller;

import java.io.IOException;

class PutNVRAMSettingsRequestPacket extends RequestPacket {
    PutNVRAMSettingsRequestPacket(DeviceSettings settings, int password) {
        super(RequestPacketType.PUT_NVRAM_DATA, settings.getType(),
                settings.getMACAddress());
        this.isDisplayOff = settings.getIsDisplayOff();
        this.brightness = settings.getBrigtness();
        if (settings.getType() == DeviceType.CURRENCY_RATE_BOARD) {
            this.userData = new UserData(settings.getUserData());
        }
        this.password = password;
    }
    
    @Override
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream res = super.makeOutputStream();
        if (getDeviceType() != DeviceType.QUEUE_BOARD) {
            res.putShort(isDisplayOff ? 1 : 0);
        }
        
        if (UDPClient.IS_STOP_BOARD) {
            res.putInt(brightness.getValue());
        }
        else {
            res.putShort(getDeviceType() != DeviceType.QUEUE_BOARD ? 
                    brightness.getValueForDevice() : brightness.getValue());
        }
        
        if (getDeviceType() == DeviceType.CURRENCY_RATE_BOARD) {
            userData.write(res);
        }
        
        if (UDPClient.IS_STOP_BOARD || getDeviceType() != DeviceType.QUEUE_BOARD) {
            res.putPassword(password);
        }
        return res;
    }
    
    private boolean isDisplayOff;
    private DisplayBrightness brightness;
    private UserData userData;
    private int password;
}
