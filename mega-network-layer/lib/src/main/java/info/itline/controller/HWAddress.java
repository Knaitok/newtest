package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;
import java.io.OutputStream;

public abstract class HWAddress {
    private String stringRep;
    private byte[] address;
    
    public abstract int getAddressSize();
    protected abstract String getAddressPartFormat();
    protected abstract String getAddressSeparatorFormat();

    HWAddress(HWAddress other) {
        address = new byte[getAddressSize()];
        System.arraycopy(other.address, 0, this.address, 0, getAddressSize());
    }
    
    public HWAddress(byte[] bytes) {
        this(bytes, 0);
    }
    
    public HWAddress(byte[] stream, int offset) {
        assert(getAddressSize() >= 4);
        assert(stream.length >= getAddressSize() + offset);
        address = new byte[getAddressSize()];
        System.arraycopy(stream, offset, address, 0, getAddressSize());
    }

    HWAddress(LittleEndianDataInputStream data) throws IOException {
        address = new byte[getAddressSize()];
        data.read(address);
    }
    
    void writeToStream(OutputStream s) throws IOException {
        s.write(address);
    }

    @Override
    public String toString() {
        if (stringRep == null) {
            StringBuilder res = new StringBuilder();
            for (int i = 0; i < getAddressSize() - 1; i++) {
                res.append(String.format(getAddressPartFormat() + 
                        getAddressSeparatorFormat(), address[i] & 0xFF));
            }
            res.append(String.format(getAddressPartFormat(), address[getAddressSize() - 1] & 0xFF));
            stringRep = res.toString();
        }
        return stringRep;
    }

    @Override
    public boolean equals(Object other) {
        if (other.getClass() != this.getClass())
            return false;
        HWAddress otherAddress = (HWAddress) other;
        for (int i = 0; i < getAddressSize(); i++) {
            if (otherAddress.address[i] != this.address[i]) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
