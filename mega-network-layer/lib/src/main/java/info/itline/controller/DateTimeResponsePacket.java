package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DateTimeResponsePacket extends ResponsePacket {
    DateTimeResponsePacket(LittleEndianDataInputStream data) throws Exceptions.InvalidPacket {
        super(data);
        
        checkPacketType(ResponsePacketType.DATE_TIME_SETTINGS, logger);
        // checkDeviceType(logger);
        try {
            if (UDPClient.IS_MEGA) {
                data.readInt();
            }
            dataChangePeriod = data.readLong() & 0x7FFFFFFF;
        }
        catch (IOException exc) {
            handleIOException(exc, logger);
        }
    }
    
    public long getDateTime() {
        return dataChangePeriod;
    }
        
    private long dataChangePeriod;
        
    private static final Logger logger = LoggerFactory.getLogger(
            DateTimeResponsePacket.class);
}
