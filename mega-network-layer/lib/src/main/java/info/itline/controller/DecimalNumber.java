package info.itline.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DecimalNumber {
    public static int parse(String s, int digitCount, int dotPosition) {
        Pattern pat = Pattern.compile(String.format("([0-9]{1,%d})\\.([0-9]{1,%d})", 
                digitCount - dotPosition, dotPosition));
        Matcher m = pat.matcher(s);
        if (!m.matches()) {
            throw new IllegalArgumentException(s + " не является допустимым значением");
        }
        String a = m.group(1);
        String b = m.group(2);
        int x = Integer.parseInt(a);
        int y = Integer.parseInt(b) * pow10(dotPosition - b.length());
        int result = x * pow10(dotPosition) + y;
        int maxValue = UserData.IS_EXTENDED_BOARD ? 0x7FFFFFFF : 0xFFFF;
        if (result > maxValue) {
            throw new IllegalArgumentException("Число " + s + " слишком большое");
        }
        return result;
    }
    
    public static String format(int x, int dotPosition) {
        int divisor = pow10(dotPosition);
        return String.format("%d.%0" + dotPosition + "d", x / divisor, x % divisor);
    }
    
    private static int pow10(int y) {
        int result = 1;
        for (int i = 0; i < y; i++) {
            result *= 10;
        }
        return result;
    }
    
    private static final Logger logger = LoggerFactory.getLogger(DecimalNumber.class);
    // private static final Pattern DIGIT_PATTERN = Pattern.compile("^[0-9]+\\.[0-9]+$");
}
