package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

class ResponsePacket {
    private MACAddress macAddress;
    private ResponsePacketType responsePacketType;
    private DeviceType deviceType;

    private static final Logger logger = LoggerFactory.getLogger(ResponsePacket.class);

    ResponsePacket(LittleEndianDataInputStream data) throws Exceptions.InvalidPacket {
        try {
            macAddress = new MACAddress(data);
            int responsePacketTypeByte = data.readUnsignedByte();
            responsePacketType = ResponsePacketType.getById(responsePacketTypeByte);
            if (responsePacketType == null) {
                Exceptions.InvalidPacket te = new Exceptions.InvalidPacket(
                        "Некорретный тип пакета: "  + responsePacketTypeByte);
                logger.error(te.getMessage());
                throw te;
            }
            int deviceTypeByte = data.readUnsignedByte();
            deviceType = DeviceType.getById(deviceTypeByte);
            if (deviceType == null) {
                Exceptions.InvalidPacket te = new Exceptions.InvalidPacket(
                        "Некорректный тип устройства: "  + deviceTypeByte);
                logger.error(te.getMessage());
                throw te;
            }
        }
        catch (IOException exc) {
            Exceptions.InvalidPacket te = new Exceptions.InvalidPacket(
                        "Ошибка при чтении данных: " + exc.getMessage());
            logger.error(te.getMessage());
            throw te;
        }
    }
    
    MACAddress getMACAddress() {
        return macAddress;
    }
    
    ResponsePacketType getResponsePacketType() {
        return responsePacketType;
    }
    
    DeviceType getDeviceType() {
        return deviceType;
    }
    
    @Override
    public String toString() {
        return String.format("[MAC = %s, Packet Type = %s, Device Type = %s]",
                macAddress, responsePacketType, deviceType);
    }
    
    protected void checkPacketType(ResponsePacketType expected, Logger logger) 
            throws Exceptions.InvalidPacket {
        if (getResponsePacketType() != expected) {
            Exceptions.InvalidPacket te = new Exceptions.InvalidPacket(
                    "Expected " + expected + " packet, got " + getResponsePacketType());
            logger.error(te.getMessage());
            throw te;
        }
    }
    
    /* protected void checkDeviceType(Logger logger) throws Exceptions.InvalidPacket {
        if (getDeviceType() != DeviceType.ETHERNET_DISPLAY_CONTROLLER) {
            Exceptions.InvalidPacket te = new Exceptions.InvalidPacket(
                    "Bad device type " + getDeviceType());
            logger.severe(te.getMessage());
            throw te;
        }
    } */
    
    protected void handleIOException(IOException exc, Logger logger) 
            throws Exceptions.InvalidPacket {
        Exceptions.InvalidPacket te = new Exceptions.InvalidPacket(
                    "IOException while reading packet: " + exc.getMessage());
        logger.error(te.getMessage());
        throw te;
    }
}
