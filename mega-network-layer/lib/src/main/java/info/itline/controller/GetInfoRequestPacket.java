package info.itline.controller;

public class GetInfoRequestPacket extends RequestPacket {
    public GetInfoRequestPacket(DeviceSettings device) {
        super(RequestPacketType.GET_INFO, device.getType(), 
                device.getMACAddress());
    }
    
    public GetInfoRequestPacket(DeviceType type, MACAddress address) {
        super(RequestPacketType.GET_INFO, type, 
                address);
    }
}