package info.itline.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public enum DataSource implements Serializable {
    NONE(0, 1, 1, "Не определен"),
    RTC(1, 1, 4, "Часы"),
    TEMPERATURE(2, 1, 4, "Температура"),
    ILLUMINATION(3, 1, 4, "Освещенность"),
    PRESSURE(4, 1, 4, "Давление"),
    HUMIDITY(5, 1, 4, "Влажность"),
    RADIATION(6, 1, 4, "Радиация"),
    PERSISTENT_STRING(7, 2, 1024, "Энергонез. строка"),
    TRANSIENT_STRING(8, 32, 128, "Опер. строка"),
    CONST_STRING(9, 32, 128, "Пост. строка"),
    PERSISTENT_INT(10, 256, 4, "Энергонез. число"),
    TRANSIENT_INT(11, 256, 4, "Опер. число"),
    CONST_INT(12, 256, 4, "Пост. число"),
    TIMER(13, 64, 4, "Таймер"),
    PERSISTENT_IMAGE(14, 2, 1024, "Энергонеp. изобр."),
    TRANSIENT_IMAGE(15, 32, 128, "Опер. изобр."),
    CONST_IMAGE(16, 32, 128, "Пост. изобр."),
    STOP_LINE(14, 1, 32, "Ост. табло: строка"),
    STOP_ROUTE(15, 10, 32, "Ост. табло: описание"),
    STOP_STOP_NAME(16, 1, 32, "Ост. табло: название"),
    STOP_TIME_REMAINIG_TIME(17, 10, 32, "Ост. табло: время в минутах"),
    STOP_TIME_REMAINING_STRING(18, 10, 32, "Ост. табло: время строкой"),
    STOP_LOGO(19, 1, 16, "Ост. табло: изображение");

    private int mId;
    private int mItemCount;
    private int mSize;
    private String mDescr;
    private static final Map<Integer, DataSource> map = new HashMap();

    private DataSource(int id, int itemCount, int size, String descr) {
        this.mId = id;
        this.mItemCount = itemCount;
        this.mSize = size;
        this.mDescr = descr;
    }

    public int getId() {
        return this.mId;
    }

    public int getItemCount() {
        return this.mItemCount;
    }

    public int getSize() {
        return this.mSize;
    }

    public String toString() {
        return this.mDescr;
    }

    public String getName() {
        return super.toString();
    }

    public static DataSource getById(int id) throws ClassNotFoundException {
        DataSource result = (DataSource) map.get(Integer.valueOf(id));
        if (result == null) {
            throw new ClassNotFoundException();
        } else {
            return result;
        }
    }

    static {
        DataSource[] var0 = values();
        int var1 = var0.length;

        for (int var2 = 0; var2 < var1; ++var2) {
            DataSource d = var0[var2];
            map.put(Integer.valueOf(d.getId()), d);
        }

    }
}

