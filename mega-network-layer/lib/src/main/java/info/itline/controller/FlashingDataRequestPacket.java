package info.itline.controller;

import java.io.IOException;

class FlashingDataRequestPacket extends RequestPacket {
    
    FlashingDataRequestPacket(DeviceSettings settings, FlashingTarget target, int address, byte[] data, int off) {
        super(RequestPacketType.FLASHING_DATA, settings.getType(), 
                settings.getMACAddress());
        assert(data.length >= off + DATA_BLOCK_SIZE);
        this.target = target;
        this.address = address;
        this.data = data;
        this.off = off;
    }
    
    @Override
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream res = super.makeOutputStream();
        res.putInt(address);
        res.putInt(target.id);
        res.write(data, off, DATA_BLOCK_SIZE);
        return res;
    }
    
    static int DATA_BLOCK_SIZE = 1024;
    
    private FlashingTarget target;
    private int address;
    private byte[] data;
    private int off;
}
