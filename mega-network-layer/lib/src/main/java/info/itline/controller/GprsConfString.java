package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

public class GprsConfString extends StringData {
    
    public GprsConfString(String s) {
        super(s);
    }

    public GprsConfString(LittleEndianDataInputStream in) throws IOException {
        super(in);
    }

    public GprsConfString(StringData other) {
        super(other);
    }

    @Override
    protected int getMaxSize() {
        return 64;
    }
}
