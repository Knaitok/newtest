package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

public class HostNameString extends StringData {

    public HostNameString(String s) {
        super(s);
    }

    public HostNameString(LittleEndianDataInputStream in) throws IOException {
        super(in);
    }

    public HostNameString(StringData other) {
        super(other);
    }

    @Override
    protected int getMaxSize() {
        return 128;
    }
}
