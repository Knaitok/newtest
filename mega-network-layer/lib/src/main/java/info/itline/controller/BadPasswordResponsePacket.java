package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class BadPasswordResponsePacket extends ResponsePacket {

    private static final Logger logger = LoggerFactory.getLogger(BadPasswordResponsePacket.class);

    BadPasswordResponsePacket(LittleEndianDataInputStream data) throws Exceptions.InvalidPacket {
        super(data);
        checkPacketType(ResponsePacketType.BAD_PASSWORD, logger);
        // checkDeviceType(log);
    }
}
