package info.itline.controller;

import java.io.IOException;

class SearchRequestPacket extends RequestPacket {
    SearchRequestPacket(DeviceType deviceType) {
        super(RequestPacketType.SEARCH, deviceType, MACAddress.BROADCAST_ADDRESS);
    }
    
    /* static SearchRequestPacket getInstance() {
        if (instance == null)
            instance = new SearchRequestPacket();
        return instance;
    }
    
    private static SearchRequestPacket instance; */

    @Override
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream s = super.makeOutputStream();
        if (UDPClient.IS_MEGA) {
            s.putInt(0);
        }
        return s;
    }
}