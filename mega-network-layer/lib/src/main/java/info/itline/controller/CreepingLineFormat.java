package info.itline.controller;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

public final class CreepingLineFormat {
    
    public static String toHtmlDocument(String in) {
        return "<html><body style=\"color: #FF7777; font-size: large;\">" + 
                toHtmlBody(in) + "</body></html>";
    }          
    
    public static String toHtmlBody(String in) throws IllegalArgumentException {
        StringInputStream s = new StringInputStream(in);
        List<HtmlTag> tagStack = new LinkedList<>();
        StringBuilder result = new StringBuilder();
        while (true) {
            if (!s.hasNextChar()) {
                break;
            }
            else {
                int c = s.nextChar();
                if (c == '#') {
                    if (!processTag(s, tagStack, result)) {
                        result.append((char) c);
                    }
                }
                else {
                    result.append((char) c);
                }
            }
        }
        for (int i = tagStack.size() - 1; i >= 0; i--) {
            result.append(tagStack.get(i).getClosingString());
        }
        return result.toString();
    }
    
    private static boolean processTag(StringInputStream s, List<HtmlTag> tagStack, StringBuilder result) {
        s.markPosition();
        if (!s.hasNextUnsignedNumber()) {
            return false;
        }
        int number = s.nextUnsignedNumber();
        if (!s.hasNextChar()) {
            s.rollback();
            return false;
        }
        int code = s.nextChar();
        
        Font font;
        if (code == Font.CODE && (font = Font.getById(number)) != null) {
            processFontTag(font, tagStack, result);
            return true;
        }
        TextProperty prop;
        if ((prop = TextProperty.getByCode(code)) != null && (number == 0 || number == 1)) {
            processTextPropertyTag(prop, number == 0 ? false : true, tagStack, result);
            return true;
        }
        DateTimePlaceholder ph;
        if ((ph = DateTimePlaceholder.find(number, code)) != null) {
            result.append(ph.getExampleValue());
            return true;
        }
        SensorPlaceholder sp;
        if ((sp = SensorPlaceholder.find(number, code)) != null) {
            result.append(sp.getExampleValue());
            return true;
        }
        
        s.rollback();
        return false;
    }
        
    private static void processFontTag(Font font, List<HtmlTag> tagStack, StringBuilder result) {
        HtmlTag lastFontTag = null;
        int lastFontTagIdx = 0;
        for (int i = tagStack.size() - 1; i >= 0; i--) {
            HtmlTag t = tagStack.get(i);
            if (t.getType() == HtmlTag.Type.FONT) {
                lastFontTag = t;
                lastFontTagIdx = i;
                break;
            }
        }
        if (lastFontTag != null) {
            closeTag(lastFontTagIdx, lastFontTag, tagStack, result);
        }
        result.append(font.getHtmlTag().getOpeningString());
        tagStack.add(font.getHtmlTag());
    }
    
    private static void processTextPropertyTag(TextProperty prop, boolean enable, List<HtmlTag> tagStack, StringBuilder result) {
        HtmlTag lastMatchingTag = null;
        int lastMatchingTagIdx = 0;
        for (int i = tagStack.size() - 1; i >= 0; i--) {
            HtmlTag t = tagStack.get(i);
            if (t.equals(prop.getHtmlTag())) {
                lastMatchingTag = t;
                lastMatchingTagIdx = i;
                break;
            }
        }
        if (lastMatchingTag != null && !enable) {
            closeTag(lastMatchingTagIdx, lastMatchingTag, tagStack, result);
        }
        else if (lastMatchingTag == null && enable) {
            result.append(prop.getHtmlTag().getOpeningString());
            tagStack.add(prop.getHtmlTag());
        }
    }
    
    private static void closeTag(int idx, HtmlTag tag, List<HtmlTag> tagStack, StringBuilder result) {
        for (int i = tagStack.size() - 1; i >= idx; i--) {
            result.append(tagStack.get(i).getClosingString());
        }
        for (int i = idx + 1; i < tagStack.size(); i++) {
            result.append(tagStack.get(i).getOpeningString());
        }
        tagStack.remove(idx);
    }
    
    private static class StringInputStream {

        public StringInputStream(String s) {
            buf = s.toCharArray();
            pos = 0;
            mark = 0;
        }        
        
        boolean hasNextChar() {
            return pos < buf.length;
        }
        
        void markPosition() {
            mark = pos;
        }
        
        void rollback() {
            pos = mark;
        }
        
        int nextChar() {
            if (pos >= buf.length) {
                return -1;
            }
            else {
                return buf[pos++];
            }
        }
        
        boolean hasNextUnsignedNumber() {
            return hasNextChar() && Character.isDigit(buf[pos]);
        }
        
        int nextUnsignedNumber() {
            if (!hasNextUnsignedNumber()) {
                return -1;
            }
            int startPos = pos;
            while (pos < buf.length && Character.isDigit(buf[pos])) {
                pos++;
            }
            String s = new String(buf, startPos, pos - startPos);
            return Integer.parseInt(s);
        }
        
        private char[] buf;
        private int pos;
        private int mark;
    }
    
    private static class HtmlTag {
        HtmlTag(String name, String attrs, Type type) {
            id = nextId++;
            this.name = name;
            this.attrs = attrs;
            this.type = type;
        }
        
        String getOpeningString() {
            if (name != null)
                return "<" + name + (attrs != null ? " " + attrs : "") + ">";
            else
                return "";
        }
        
        String getClosingString() {
            if (name != null)
                return "</" + name + ">";
            else
                return "";
        }
        
        Type getType() {
            return type;
        }
        
        @Override
        public boolean equals(Object o) {
            if (!this.getClass().equals(o.getClass())) {
                return false;
            }
            return this.id == ((HtmlTag) o).id;
        }
        
        enum Type {
            FONT, TEXT_PROPERTY;
        }
        
        private static int nextId = 0;
        
        private int id;
        private String name, attrs;
        private Type type;
    }
    
    public static enum Font {
        MONOSPACE                   (0, "Моноширинный", new HtmlTag(null, null, HtmlTag.Type.FONT)),
        NARROW_PROPORTIONAL         (1, "Пропорциональный узкий", new HtmlTag("i", null, HtmlTag.Type.FONT)),
        WIDE_PROPORTIONAL           (2, "Пропорциональный широкий", new HtmlTag("b", null, HtmlTag.Type.FONT));
        
        private Font(int id, String name, HtmlTag htmlTag) {
            this.tag = formatTag(id, CODE);
            this.id = id;
            this.name = name;
            this.htmlTag = htmlTag;
        }
        
        static Font getById(int id) {
            for (Font f: Font.values()) {
                if (f.id == id) {
                    return f;
                }
            }
            return null;
        }
        
        public String getTag() {
            return tag;
        }
        
        public String getName() {
            return name;
        }
        
        HtmlTag getHtmlTag() {
            return htmlTag;
        }
        
        @Override
        public String toString() {
            return name;
        }
        
        private final String name;
        private final String tag;
        private final int id;
        private HtmlTag htmlTag;
        
        private static final char CODE = 'f';
    }
    
    public static enum TextProperty {
        UNDERLINE('u', new HtmlTag("u", null, HtmlTag.Type.TEXT_PROPERTY)),
        BLINK('h', new HtmlTag("span", "style=\"color: #790000\"", HtmlTag.Type.TEXT_PROPERTY)),
        INVERSE('e', new HtmlTag("span", "style=\"background-color: #CDCDCD\";", HtmlTag.Type.TEXT_PROPERTY)),
        LOW_BRIGTHNESS('y', new HtmlTag("span", "style=\"color: #FFCACA\";", HtmlTag.Type.TEXT_PROPERTY));
        
        TextProperty(char code, HtmlTag htmlTag) {
            startTag = formatTag(1, code);
            stopTag = formatTag(0, code);
            this.code = code;
            this.htmlTag = htmlTag;
        }
        
        static TextProperty getByCode(int code) {
            for (TextProperty p: TextProperty.values()) {
                if (p.code == code) {
                    return p;
                }
            }
            return null;
        }
        
        public String getStartTag() {
            return startTag;
        }
        
        public String getStopTag() {
            return stopTag;
        }
        
        HtmlTag getHtmlTag() {
            return htmlTag;
        }
        
        @Override
        public String toString() {
            return startTag + "/" + stopTag;
        }
        
        private String startTag, stopTag;
        private char code;
        private HtmlTag htmlTag;
    }
    
    public static enum DateTimePlaceholder {
        HOUR_MINUTE(4, 't', "ЧЧ:МН", makeFmt("HH:mm")),
        HOUR_MINUTE_SECOND(6, 't', "ЧЧ:МН:СС", makeFmt("HH:mm:ss")),
        DAY_OF_WEEK(2, 'd', "НН", makeFmt("EE")),
        DAY_OF_MONTH_MONTH(4, 'd', "ДД.МС", makeFmt("dd.MM")),
        DAY_OF_WEEK_MONTH_YEAR_2(6, 'd', "ДД.МС.ГГ", makeFmt("dd.MM.yy")),
        DAY_OF_WEEK_MONTH_YEAR_4(8, 'd', "ДД.МС.ГГГГ", makeFmt("dd.MM.yyyy")),
        DAY_OF_WEEK_DAY_OF_MONTH_MONTH_YEAR_4(10, 'd', "НН ДД.МС.ГГГГ", makeFmt("EE dd.MM.yyyy")),
        DAY_OF_WEEK_DAY_OF_MONTH_MONTH_YEAR_2(12, 'd', "НН ДД.МС.ГГ", makeFmt("EE dd.MM.yy")),
        DAY_OF_WEEK_DAY_OF_MONTH_MONTH(14, 'd', "НН ДД.МС", makeFmt("EE dd.MM"));

        private DateTimePlaceholder(int id, char code, String description,
                SimpleDateFormat format) {
            this.tag = formatTag(id, code);
            this.code = code;
            this.id = id;
            this.description = description;
            this.format = format;
        }
        
        public String getTag() {
            return tag;
        }
        
        public String getDescription() {
            return description;
        }
        
        @Override
        public String toString() {
            return description;
        }

        static DateTimePlaceholder find(int number, int code) {
            if (CODES.indexOf(code) == -1) {
                return null;
            }
            for (DateTimePlaceholder p: DateTimePlaceholder.values()) {
                if (p.id == number && p.code == code) {
                    return p;
                }
            }
            return null;
        }
        
        private static SimpleDateFormat makeFmt(String s) {
            return new SimpleDateFormat(s);
        }
        
        String getExampleValue() {
            if (exampleValue == null) {
                exampleValue = format.format(exampleDate.getTime());
            }
            return exampleValue;
        }
        
        private int id;
        private char code;
        private String tag;
        private String description;
        private SimpleDateFormat format;
        private static final String CODES = "dt";
        private static final GregorianCalendar exampleDate = 
                new GregorianCalendar();
        private String exampleValue;
    }
    
    private static String formatTag(int value, char code) {
        return "#" + value + code;
    }
    
    private static String formatTagTwoDigit(int value, char code) {
        return String.format("#%02d%c", value, code);
    }
    
    public enum SensorPlaceholder {
        FULL_SIZE(0, "Полностью", "-21°C"),
        TWO_SYMBOLS(2, "Два символа", "21"),
        THREE_SYMBOLS(3, "Три символа", "-21");
        
        private SensorPlaceholder(int size, String description, String exampleValue) {
            this.size = size;
            this.description = description;
            this.exampleValue = exampleValue;
        }
        
        public String tagForSensorNo(int no) {
            return formatTagTwoDigit(no * 10 + size, CODE);
        }
        
        @Override
        public String toString() {
            return description;
        }
        
        String getExampleValue() {
            return exampleValue;
        }
        
        static SensorPlaceholder find(int number, int code) {
            if (code != CODE) {
                return null;
            }
            int size = number % 10;
            int sensorNo = number / 10;
            if (sensorNo > 9) {
                return null;
            }
            for (SensorPlaceholder s: SensorPlaceholder.values()) {
                if (size == s.size) {
                    return s;
                }
            }
            return null;
        }
        
        private int size;
        private String description;
        private String exampleValue;
        
        private static final char CODE = 'g';
    }
    
    public static final String STRING_START_PROPERTIES = formatTag(0, 'a');
}
