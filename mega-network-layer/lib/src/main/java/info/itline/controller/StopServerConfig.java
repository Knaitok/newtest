package info.itline.controller;

import java.util.Objects;

public class StopServerConfig {

    public StopServerConfig()
    {
        
    }
    
    StopServerConfig(StopServerConfig o) {
        this.safe = o.safe;
        this.tempMin = o.tempMin;
        this.tempMax = o.tempMax;
        this.updatePeriod = o.updatePeriod;
        this.watchdogPeriod = o.watchdogPeriod;
        this.port = o.port;
        this.id = o.id;
        this.gprsAccessPoint = new GprsConfString(o.gprsAccessPoint);
        this.gprsLogin = new GprsConfString(o.gprsLogin);
        this.gprsPassword = new GprsConfString(o.gprsPassword);
        this.serverName = new ServerNameString(o.serverName);
        this.hostName = new HostNameString(o.hostName);
        this.pageOnServer = new PageOnServerString(o.pageOnServer);
    }
    
    public int getSafe() {
        return safe;
    }

    public void setSafe(int safe) {
        this.safe = safe;
    }

    public short getTempMin() {
        return tempMin;
    }

    public void setTempMin(short tempMin) {
        this.tempMin = tempMin;
    }

    public short getTempMax() {
        return tempMax;
    }

    public void setTempMax(short tempMax) {
        this.tempMax = tempMax;
    }

    public int getUpdatePeriod() {
        return updatePeriod;
    }

    public void setUpdatePeriod(int updatePeriod) {
        this.updatePeriod = updatePeriod;
    }

    public int getWatchdogPeriod() {
        return watchdogPeriod;
    }

    public void setWatchdogPeriod(int watchdogPeriod) {
        this.watchdogPeriod = watchdogPeriod;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getZero() {
        return zero;
    }

    public void setZero(int zero) {
        this.zero = zero;
    }

    public ServerNameString getServerName() {
        return serverName;
    }

    public void setServerName(ServerNameString serverName) {
        this.serverName = serverName;
    }

    public PageOnServerString getPageOnServer() {
        return pageOnServer;
    }

    public void setPageOnServer(PageOnServerString pageOnServer) {
        this.pageOnServer = pageOnServer;
    }

    public HostNameString getHostName() {
        return hostName;
    }

    public void setHostName(HostNameString hostName) {
        this.hostName = hostName;
    }

    public GprsConfString getGprsAccessPoint() {
        return gprsAccessPoint;
    }

    public void setGprsAccessPoint(GprsConfString gprsAccessPoint) {
        this.gprsAccessPoint = gprsAccessPoint;
    }

    public GprsConfString getGprsLogin() {
        return gprsLogin;
    }

    public void setGprsLogin(GprsConfString gprsLogin) {
        this.gprsLogin = gprsLogin;
    }

    public GprsConfString getGprsPassword() {
        return gprsPassword;
    }

    public void setGprsPassword(GprsConfString gprsPassword) {
        this.gprsPassword = gprsPassword;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.safe;
        hash = 29 * hash + this.tempMin;
        hash = 29 * hash + this.tempMax;
        hash = 29 * hash + this.updatePeriod;
        hash = 29 * hash + this.watchdogPeriod;
        hash = 29 * hash + this.port;
        hash = 29 * hash + this.id;
        hash = 29 * hash + this.zero;
        hash = 29 * hash + Objects.hashCode(this.gprsAccessPoint);
        hash = 29 * hash + Objects.hashCode(this.gprsLogin);
        hash = 29 * hash + Objects.hashCode(this.gprsPassword);
        hash = 29 * hash + Objects.hashCode(this.serverName);
        hash = 29 * hash + Objects.hashCode(this.pageOnServer);
        hash = 29 * hash + Objects.hashCode(this.hostName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StopServerConfig other = (StopServerConfig) obj;
        if (this.safe != other.safe) {
            return false;
        }
        if (this.tempMin != other.tempMin) {
            return false;
        }
        if (this.tempMax != other.tempMax) {
            return false;
        }
        if (this.updatePeriod != other.updatePeriod) {
            return false;
        }
        if (this.watchdogPeriod != other.watchdogPeriod) {
            return false;
        }
        if (this.port != other.port) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        if (this.zero != other.zero) {
            return false;
        }
        if (!Objects.equals(this.gprsAccessPoint, other.gprsAccessPoint)) {
            return false;
        }
        if (!Objects.equals(this.gprsLogin, other.gprsLogin)) {
            return false;
        }
        if (!Objects.equals(this.gprsPassword, other.gprsPassword)) {
            return false;
        }
        if (!Objects.equals(this.serverName, other.serverName)) {
            return false;
        }
        if (!Objects.equals(this.pageOnServer, other.pageOnServer)) {
            return false;
        }
        if (!Objects.equals(this.hostName, other.hostName)) {
            return false;
        }
        return true;
    }
    
    private int safe;
    private short tempMin, tempMax;
    private int updatePeriod, watchdogPeriod;
    private int port, id, zero = 0;
    private GprsConfString gprsAccessPoint, gprsLogin, gprsPassword;
    private ServerNameString serverName;
    private PageOnServerString pageOnServer;
    private HostNameString hostName;
}
