package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class StopServerConfigResponsePacket extends ResponsePacket {

    public StopServerConfigResponsePacket(LittleEndianDataInputStream data) throws Exceptions.InvalidPacket {
        super(data);
        checkPacketType(ResponsePacketType.DYNAMIC_DATA, logger);
        try {
            serverConfig = new StopServerConfig();
            
            serverConfig.setSafe(data.readInt());
            serverConfig.setTempMin(data.readShort());
            serverConfig.setTempMax(data.readShort());
            
            serverConfig.setUpdatePeriod(data.readInt());
            serverConfig.setWatchdogPeriod(data.readInt());
            
            serverConfig.setPort(data.readInt());
            serverConfig.setId(data.readInt());
            serverConfig.setZero(data.readInt());
            
            serverConfig.setGprsAccessPoint(new GprsConfString(data));
            serverConfig.setGprsLogin(new GprsConfString(data));
            serverConfig.setGprsPassword(new GprsConfString(data));
            
            serverConfig.setServerName(new ServerNameString(data));
            serverConfig.setPageOnServer(new PageOnServerString(data));
            serverConfig.setHostName(new HostNameString(data));
        }
        catch (IOException e) {
            handleIOException(e, logger);
        }
    }
    
    public StopServerConfig getStopServerConfig() {
        return serverConfig;
    }
    
    private StopServerConfig serverConfig;
    
    private static final Logger logger = LoggerFactory.getLogger(
            ServerConfigResponsePacket.class);
}
