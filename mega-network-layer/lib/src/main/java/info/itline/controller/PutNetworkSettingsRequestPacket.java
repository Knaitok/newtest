package info.itline.controller;

import java.io.IOException;

class PutNetworkSettingsRequestPacket extends RequestPacket {
    public PutNetworkSettingsRequestPacket(DeviceSettings device, int password) {
        super(RequestPacketType.PUT_NETWORK_SETTINGS, 
                device.getType(), 
                device.getMACAddress());
        this.name = new DeviceName(device.getName());
        this.ip = new IPAddress(device.getIp());
        this.mask = new IPAddress(device.getMask());
        this.gateway = new IPAddress(device.getGateway());
        this.password = password & 0xFFFF;
    }
    
    @Override
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream res = super.makeOutputStream();
        
        name.write(res);
        ip.writeToStream(res);
        mask.writeToStream(res);
        gateway.writeToStream(res);        
        if (UDPClient.IS_STOP_BOARD || getDeviceType() != DeviceType.QUEUE_BOARD) {
            res.putPassword(password);
        }
        return res;
    }
    
    private DeviceName name;
    private IPAddress ip;
    private IPAddress mask;
    private IPAddress gateway;
    private int password;
}
