package info.itline.tz_0816_36.controller

import info.itline.mega.net.Client
import info.itline.mega.net.DeviceId
import info.itline.mega.ui.Form
import info.itline.mega.ui.PopupHelper
import info.itline.mega.ui.SettingsGroup
import info.itline.mega.ui.UpdateOperations
import info.itline.tz_0816_36.model.DurationCalculator
import info.itline.tz_0816_36.model.FontSize
import info.itline.tz_0816_36.model.InfoStringMode
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.control.ComboBox
import javafx.scene.control.DatePicker
import javafx.scene.control.TextField
import javafx.scene.input.KeyEvent
import javafx.scene.layout.VBox
import javafx.stage.Window
import javafx.util.StringConverter
import java.io.IOException
import java.net.URL
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * This is class controller for Main.fxml file. It provides changing the data
 * on the days counter application.
 */
class MainController : SettingsGroup() {

    companion object {
        val DATA_PATTERN = "dd.MM.yyyy"

        val DIGIT_PATTERN = "[0-9]".toRegex()

        fun load(): Form<MainController> {
            try {
                val loader = FXMLLoader(MainController::class.java.getResource("/Main.fxml"))
                val root = loader.load<VBox>()
                val controller = loader.getController<MainController>()
                return Form(controller, root)
            } catch (e: IOException) {
                e.printStackTrace()
                throw RuntimeException("This should have never happen")
            }
        }
    }

    /**
     * This is base formatter for date.
     */
    private var dateFormatter = DateTimeFormatter.ofPattern(DATA_PATTERN)

    /**
     * This is the link to the array of date pickers for easy access to this fields.
     */
    private lateinit var datePickers: Array<DatePicker>

    /**
     * A set of text fields that contain a duration in days
     */
    private lateinit var textFields: Array<TextField>

    /**
     * Indexes of strings on the panel.
     */
    private val datesIndexes = shortArrayOf(0, 1, 2, 3)

    /**
     * Initializes the fxml view for the first time with max values.
     */
    override fun initialize(location: URL?, resources: ResourceBundle?) {
        stringTypeComboBox.items.addAll(InfoStringMode.values())
        fontSizeComboBox.items.addAll(FontSize.values())
        stringTypeComboBox.value = InfoStringMode.DYNAMIC_FAST
        fontSizeComboBox.value = FontSize.FONT_8
        datePickers = arrayOf(forestAffiliateTrashes,
                affiliateTrashes, subfirmsTraches, carTrashes)
        textFields = arrayOf(forestAffiliateTrashesCount, affiliateTrashesCount,
                subfirmsTrashesCount, carTrashesCount)
        datePickers.forEach { picker ->
            setConverterFor(picker)
            picker.value = LocalDate.now()
        }
        bindDatePickersWithTextFields()
        bindTextFieldsWithDatePickers()
    }

    /**
     * Set the converter of data for data picker.
     */
    private fun setConverterFor(picker: DatePicker) {
        val converter = object : StringConverter<LocalDate>() {
            override fun toString(date: LocalDate?): String {
                if (date != null) {
                    return dateFormatter.format(date)
                } else {
                    return ""
                }
            }

            override fun fromString(string: String?): LocalDate? {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter)
                } else {
                    return null
                }
            }
        }
        picker.converter = converter
    }


    /**
     * Bind change in pickers to change in text fields
     */
    private fun bindDatePickersWithTextFields() {
        datePickers.forEachIndexed { index, datePicker ->
            datePicker.setOnAction {
                val duration = DurationCalculator.calculateDuration(datePicker.value)
                if(textFields[index].text != duration)
                    textFields[index].text = duration
            }
        }
    }

    /**
     * Bind a change in the text field with the date picker
     */
    private fun bindTextFieldsWithDatePickers() {
        textFields.forEachIndexed { index, textField ->
            textField.addEventFilter(KeyEvent.KEY_TYPED, { keyEvent ->
                if(textField.text.length >= 4)
                    keyEvent.consume()
                if(!keyEvent.character.matches(DIGIT_PATTERN)) {
                    keyEvent.consume()
                }
            })
            textField.addEventFilter(KeyEvent.KEY_RELEASED, {
                if(textField.text.isEmpty())
                    return@addEventFilter
                val date = DurationCalculator.calculateDate(textField.text.toInt())
                if(datePickers[index].value != date)
                    datePickers[index].value = date
            })
        }
    }

    /**
     * Refiles the application fields, when user select the device.
     */
    override fun updateView(deviceId: DeviceId?) {
        if (deviceId == null)
            return
        retrieveDates()
        retrieveInfoString()
    }

    /**
     * Process the byte code of string mode.
     */
    fun processInfoStringProfile(profile: Byte): InfoStringMode {
        when (profile) {
            InfoStringMode.DYNAMIC_FAST.typeFormat.value.toByte() -> return InfoStringMode.DYNAMIC_FAST
            InfoStringMode.DYNAMIC_MEDIUM.typeFormat.value.toByte() -> return InfoStringMode.DYNAMIC_MEDIUM
            InfoStringMode.DYNAMIC_LOW.typeFormat.value.toByte() -> return InfoStringMode.DYNAMIC_LOW
            else -> return InfoStringMode.STATIC
        }
    }

    private fun retrieveInfoString() {
        val client = Client.getInstance()
        client.getPersistentString(deviceId, 0, Client.OnReceive { packet ->
            val stringData = FontSize.extractFontAndString(packet.string)
            fontSizeComboBox.value = stringData.first
            messageTextField.text = stringData.second
            stringTypeComboBox.value = processInfoStringProfile(packet.profile)
        }, defaultErrorHandler)
    }

    private fun sendInfoString() {
        val client = Client.getInstance()
        val text = fontSizeComboBox.value.formatString(messageTextField.text.orEmpty())
        client.setPersistentString(deviceId, 0,
                stringTypeComboBox.selectionModel.selectedItem.typeFormat.value.toByte(),
                text, Client.OnReceive { PopupHelper.showPopup("Сохранено", window) },
                defaultErrorHandler)
    }

    /**
     * Send date instants to the panel
     */
    private fun sendDateInstants() {
        textFields.forEachIndexed { index, textField ->
            if(textField.text.isEmpty()) {
                textField.text = "0"
            }
            val date = DurationCalculator.calculateDate(textField.text.toInt())
            if(datePickers[index].value != date)
                datePickers[index].value = date
        }
        val instants = datePickers.map {
            DurationCalculator.convertDateToInstant(it.value)
        }.toIntArray()
        Client.getInstance().setPersistentNumber(deviceId, datesIndexes, instants, Client.OnReceive {},
                defaultErrorHandler)
    }

    /**
     * Get dates from the panel and set them onto the window
     */
    private fun retrieveDates() {
        Client.getInstance().getPersistentNumber(deviceId, datesIndexes, Client.OnReceive { packet ->
            datePickers.forEachIndexed { index, datePicker ->
                datePicker.value = DurationCalculator.convertInstantToDate(packet.values[index])
                textFields[index].text = DurationCalculator.calculateDuration(datePicker.value)
            }
        }, defaultErrorHandler)
    }

    /**
     * Sends the start date to the device memory(unvisible numbers) and
     * correct the values on the panel(visible fields and string).
     */
    @FXML
    fun sendStrings() {
        Client.getInstance().synchronizeDateTime(deviceId, Client.OnReceive {}, defaultErrorHandler)
        sendDateInstants()
        sendInfoString()
    }

    override fun getIconFilename(): String {
        return "schedule.png"
    }

    override fun getGroupName(): String {
        return "Данные"
    }

    @FXML
    private lateinit var forestAffiliateTrashesCount: TextField

    @FXML
    private lateinit var affiliateTrashesCount: TextField

    @FXML
    private lateinit var subfirmsTrashesCount: TextField

    @FXML
    private lateinit var carTrashesCount: TextField

    @FXML
    private lateinit var pane: VBox

    @FXML
    private lateinit var messageTextField: TextField

    @FXML
    private lateinit var stringTypeComboBox: ComboBox<InfoStringMode>

    @FXML
    private lateinit var fontSizeComboBox: ComboBox<FontSize>

    @FXML
    private lateinit var forestAffiliateTrashes: DatePicker

    @FXML
    private lateinit var affiliateTrashes: DatePicker

    @FXML
    private lateinit var subfirmsTraches: DatePicker

    @FXML
    private lateinit var carTrashes: DatePicker

    override fun getWindow(): Window {
        return pane.scene.window
    }

    override fun getOperationType(): UpdateOperations? {
        return null
    }
}

