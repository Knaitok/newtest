package info.itline.tz_0816_36.model

import java.time.Duration
import java.time.LocalDate

/**
 * Class contains logic to convert dates into instances
 */
class DurationCalculator {
    companion object {
        val MILLENNIUM_START: LocalDate = LocalDate.of(2000, 1, 1)

        val SECONDS_IN_DAY = 60 * 60 * 24

        val TODAY: LocalDate = LocalDate.now()

        /**
         * Represent the current date as the seconds interval between
         * the beginning of the millennium and the passed date.
         */
        fun convertDateToInstant(date: LocalDate): Int {
            return daysInPeriod(MILLENNIUM_START, date) * SECONDS_IN_DAY
        }

        /**
         * Create a local date from the instant that represents the
         * duration in seconds from the millennium start
         */
        fun convertInstantToDate(instant: Int): LocalDate {
            val duration = instant / SECONDS_IN_DAY
            return MILLENNIUM_START.plusDays(duration.toLong())
        }

        /**
         * Calculate how many days pass between passed date and today
         */
        fun calculateDuration(date: LocalDate): String {
            return daysInPeriod(date, TODAY).toString()
        }

        /**
         * Calculate the date based on the duration
         */
        fun calculateDate(duration: Int): LocalDate {
            return TODAY.minusDays(duration.toLong())
        }

        /**
         * Calculate how many days in the period
         */
        private fun daysInPeriod(startDate: LocalDate, endDate: LocalDate): Int {
            return Duration.between(startDate.atStartOfDay(), endDate.atStartOfDay())
                    .toDays().toInt()
        }
    }

}