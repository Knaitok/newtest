package info.itline.tz_0816_36

import info.itline.mega.net.DeviceType
import info.itline.mega.ui.AboutController
import info.itline.mega.ui.ApplicationLaunchHelper
import info.itline.mega.ui.BrightnessSettingsController
import info.itline.mega.ui.NetworkSettingsController
import info.itline.tz_0816_36.controller.MainController
import javafx.application.Application
import javafx.stage.Stage

class CounterApplication : Application() {
    override fun start(stage: Stage) {
        ApplicationLaunchHelper.launchApplication(this, stage,
                DeviceType.MEGA_BOARD, true, false,
                MainController.load(),
                NetworkSettingsController.load(),
                BrightnessSettingsController.load(),
                AboutController.load())
    }
}

fun main(args: Array<String>) {
    Application.launch(CounterApplication::class.java, *args)
}
